$(document).ready(function(){
    $("#txt_nomheb").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: config.base+"ajax/get_noms_hebergs",
                dataType: "json",
                data: {
                    term: request.term

                },
                success: function(data) {
                    response(data);
                }
            });
        },
        minLength: 1,
    });
});
