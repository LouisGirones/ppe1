$(document).ready(function(){

    $("#back").click(function(e) {
        e.preventDefault();
        history.back(1);
    })

 	$('#rbVil').click(function()
  	{
    	$('#txt_mail').removeAttr("disabled");
    	$('#txt_tel_fixe').removeAttr("disabled");
    	$('#txt_tel_port').removeAttr("disabled");
  	});

  	$('#rbGest').click(function()
  	{
	    $('#txt_mail').attr("disabled","disabled");
	    $('#txt_tel_fixe').attr("disabled","disabled");
	    $('#txt_tel_port').attr("disabled","disabled");
      $('span.vil-err').html("");
  	});

  	if ($("#rbGest").is(':checked')) {
    	$('#txt_mail').attr("disabled","disabled");
    	$('#txt_tel_fixe').attr("disabled","disabled");
    	$('#txt_tel_port').attr("disabled","disabled");
  	}
});
