$(function() {
    // Attach csfr data token
    $.ajaxSetup({
       data: csfrData
    });
});

var semainesDeb;

//Vérification des semaines possibles pour réserver un hébergement
$(document).ready(function(){
    $.ajax({
        url:config.base+"ajax/get_semaines",
        type:"POST",
        data: {
          saison : $('#cdSaison').val(),
          noHeb : $('#noHeb').val(),
        },
        dataType: "json",
        success: function(msg){
            console.log(msg);
            semainesDeb = msg;
        }
    })
   
});

//Calendrier de réservation (évenement click)
$('#txt_semaine_deb').datepicker({
    showAnim: "fold",
    regional: "fr",
    beforeShowDay: function(date) {// comparaison tableau JSON et date a venir du du datepicker
        //+1 sur les mois car de 0 a 11
        dmy = date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();
        //On recupere soit la place dans le tableau soit -1
        //console.log($.inArray(dmy, semainesDeb));
        if ($.inArray(dmy, semainesDeb) != -1) {
        //[bool dispo, class css, pop up]
            return [true, "dp-highlight","Disponible"];
        } 
        else {
            return [false,"","Indisponible"];
        }
    }
}); 