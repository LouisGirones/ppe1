(function( factory ) {
    if ( typeof define === "function" && define.amd ) {
      // AMD. Register as an anonymous module.
      define([ "../jquery.ui.datepicker" ], factory );
    } else {
      // Browser globals
      factory( jQuery.datepicker );
    }
}
(function( datepicker ) {
    datepicker.regional['fr'] = {
        closeText: 'Fermer',
        prevText: 'Précédent',
        nextText: 'Suivant',
        currentText: 'Aujourd\'hui',
        monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin',
          'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
        monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin',
          'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
        dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
        dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
        dayNamesMin: ['D','L','M','M','J','V','S'],
        weekHeader: 'Sem.',
        dateFormat: 'dd-mm-yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    datepicker.setDefaults(datepicker.regional['fr']);
    return datepicker.regional['fr'];
}));

/**************************************************
**********Datepicker samedi seulement**************
**************************************************/
$(".datePickerSamedi").datepicker({ 
    dateFormat:"dd-mm-yy",
    regional: "fr",
    //Objet date instancié à la valeur d'aujourd'hui (constructeur par défaut)
    beforeShowDay: function(date) {
            return [date.getDay() == 6];
        }
});

/**************************************************
**********Datepicker gest résa*********************
**************************************************/
$(".datePicker").datepicker({ 
    dateFormat:"dd-mm-yy",
    regional: "fr",
    //Objet date instancié à la valeur d'aujourd'hui (constructeur par défaut)
    maxDate: new Date
});
