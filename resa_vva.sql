-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Ven 01 Juillet 2016 à 16:01
-- Version du serveur :  5.7.9
-- Version de PHP :  5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `resa_vva`
--

-- --------------------------------------------------------

--
-- Structure de la table `compte`
--

DROP TABLE IF EXISTS `compte`;
CREATE TABLE IF NOT EXISTS `compte` (
  `USER` char(8) COLLATE utf8_bin NOT NULL,
  `MDP` char(100) COLLATE utf8_bin DEFAULT NULL,
  `NOMCOMPTE` char(40) COLLATE utf8_bin DEFAULT NULL,
  `PRENOMCOMPTE` char(40) COLLATE utf8_bin DEFAULT NULL,
  `DATEINSCRIP` date DEFAULT NULL,
  `DATESUPPRESSION` date DEFAULT NULL,
  `TYPECOMPTE` char(3) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`USER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `compte`
--

INSERT INTO `compte` (`USER`, `MDP`, `NOMCOMPTE`, `PRENOMCOMPTE`, `DATEINSCRIP`, `DATESUPPRESSION`, `TYPECOMPTE`) VALUES
('admin', '$2y$12$1HB8D942e4n92BdLurv1H.BwjD9xp4zb5r2r2p0/SMGyDWkNkd6/C', 'admin', 'admin', '2015-10-09', NULL, 'AD'),
('gest1', '$2y$12$UyIUEuMDWkHpPfGXecxi6eXkTSksxRH09289tYdAxl.hYyOonjr82', 'gest1', 'gest1', '2015-11-09', NULL, 'GES'),
('gest2', '$2y$12$7cYm82ccoQazUsFEos5Re.Jc611SHCknYUbOV47uxtEWOqZrJsW7i', 'gest2', 'gest2', '2016-05-20', NULL, 'GES'),
('gest3', '$2y$12$kWwPbm5Dv7IcQebdBcsADeT4Ty80AxhzjW7SFeB862PZ24aSSTaEG', 'gest3', 'gest3', '2016-05-20', NULL, 'GES'),
('gvbhj', '$2y$12$pNlxF0oMmrG4FCsJuAbPSeMmk5uGPYTPGZwWjnJUjlGi8A9WOeD7e', 'gvhbj', 'gvjbh', '2016-05-20', NULL, 'GES'),
('user1', '$2y$12$gcrcHnygPNvc7mtCnd4Re.AQZKqzATYcUv37a/yqepOPxpZeokkxi', 'user1', 'user1', '2015-10-27', NULL, 'VIL'),
('user2', '$2y$12$VI8ZV9EEBkMkuXRtdk3/w.u8abLekhyXzCViH4euq2OE.r8wV/ro2', 'user2', 'user2', '2015-11-24', NULL, 'VIL'),
('user3', '$2y$12$JdHHTP7Ep8chqjNHrOnajuet/IJzIVcLMTiwSEPgqXItXfYlMHC4C', 'user3', 'user3', '2015-11-25', NULL, 'VIL'),
('user4', '$2y$12$JDXw./1t9rGK9DZXjfMZzOyuuI0Xx5vd8KodAO5q9f/pS7ILLzOdG', 'user4', 'user4', '2016-05-20', NULL, 'VIL');

-- --------------------------------------------------------

--
-- Structure de la table `etat_resa`
--

DROP TABLE IF EXISTS `etat_resa`;
CREATE TABLE IF NOT EXISTS `etat_resa` (
  `CODEETATRESA` int(2) NOT NULL AUTO_INCREMENT,
  `NOMETATRESA` char(30) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`CODEETATRESA`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `etat_resa`
--

INSERT INTO `etat_resa` (`CODEETATRESA`, `NOMETATRESA`) VALUES
(1, 'Prise en compte.'),
(2, 'Accusé de réception.'),
(3, 'Arrhes versés.'),
(4, 'En cours.');

-- --------------------------------------------------------

--
-- Structure de la table `hebergement`
--

DROP TABLE IF EXISTS `hebergement`;
CREATE TABLE IF NOT EXISTS `hebergement` (
  `NOHEB` int(4) NOT NULL AUTO_INCREMENT,
  `CODETYPEHEB` int(5) NOT NULL,
  `NOMHEB` char(25) COLLATE utf8_bin DEFAULT NULL,
  `NBPLACEHEB` int(2) DEFAULT NULL,
  `SURFACEHEB` int(2) DEFAULT NULL,
  `INTERNET` tinyint(1) DEFAULT NULL,
  `ANNEEHEB` int(4) DEFAULT NULL,
  `SECTEURHEB` char(15) COLLATE utf8_bin DEFAULT NULL,
  `ORIENTATIONHEB` char(5) COLLATE utf8_bin DEFAULT NULL,
  `ETATHEB` char(32) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIHEB` varchar(900) COLLATE utf8_bin DEFAULT NULL,
  `PHOTOHEB` char(50) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`NOHEB`),
  UNIQUE KEY `NOMHEB` (`NOMHEB`),
  KEY `FK_HEBERGEMENT_TYPE_HEB` (`CODETYPEHEB`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `hebergement`
--

INSERT INTO `hebergement` (`NOHEB`, `CODETYPEHEB`, `NOMHEB`, `NBPLACEHEB`, `SURFACEHEB`, `INTERNET`, `ANNEEHEB`, `SECTEURHEB`, `ORIENTATIONHEB`, `ETATHEB`, `DESCRIHEB`, `PHOTOHEB`) VALUES
(2, 2, 'Chalet1', 6, 92, 0, 2012, 'Alpes', 'Sud', 'Bon', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. ', 'chalet1.jpg'),
(3, 2, 'Chalet2', 3, 75, 1, 2013, 'Alpes', 'Sud', 'Très bon', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. ', 'chalet2.jpg'),
(4, 2, 'Chalet3', 2, 68, 1, 2013, 'Montagne', 'Nord', 'Très bon', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. ', 'chalet3.jpg'),
(5, 1, 'Appartement1', 4, 99, 1, 2012, 'Alpes', 'Ouest', 'Très bon', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. ', 'Appartement1.jpg'),
(6, 1, 'Appartement2', 4, 95, 1, 2014, 'Alpes', 'Est', 'Très bon', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. ', 'appartement2.jpg'),
(7, 1, 'Appartement3', 4, 82, 1, 2014, 'Alpes', 'Sud', 'Très bon', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. ', 'Appartement3.jpg'),
(8, 3, 'Bungalow1', 5, 99, 1, 2013, 'Alpes', 'Nord', 'Très bon', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. ', 'bungalow1.jpg'),
(9, 3, 'Bungalow2', 3, 67, 0, 2013, 'Colline', 'Est', 'Très bon', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. ', 'Bungalow2.jpg'),
(10, 3, 'Bungalow3', 3, 72, 0, 2013, 'Montagne', 'Ouest', 'Très bon', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. Ad necessitatibus velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae repudiandae fugiat illo cupiditate excepturi esse officiis consectetur, laudantium qui voluptatem. ', 'Bungalow3.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `resa`
--

DROP TABLE IF EXISTS `resa`;
CREATE TABLE IF NOT EXISTS `resa` (
  `NOHEB` int(4) NOT NULL,
  `DATEDEBSEM` date NOT NULL,
  `NOVILLAGEOIS` int(5) NOT NULL,
  `CODEETATRESA` int(2) NOT NULL,
  `DATERESA` date DEFAULT NULL,
  `DATEACCUSERECEPT` date DEFAULT NULL,
  `DATEARRHES` date DEFAULT NULL,
  `MONTANTARRHES` decimal(7,2) DEFAULT NULL,
  `NBOCCUPANT` int(2) DEFAULT NULL,
  `PRIXRESA` decimal(7,2) DEFAULT NULL,
  PRIMARY KEY (`NOHEB`,`DATEDEBSEM`),
  KEY `FK_RESA_VILLAGEOIS` (`NOVILLAGEOIS`),
  KEY `FK_RESA_SEMAINE` (`DATEDEBSEM`),
  KEY `FK_RESA_ETAT_RESA` (`CODEETATRESA`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `resa`
--

INSERT INTO `resa` (`NOHEB`, `DATEDEBSEM`, `NOVILLAGEOIS`, `CODEETATRESA`, `DATERESA`, `DATEACCUSERECEPT`, `DATEARRHES`, `MONTANTARRHES`, `NBOCCUPANT`, `PRIXRESA`) VALUES
(5, '2016-08-20', 2, 2, '2016-05-24', '2016-05-25', NULL, NULL, 4, '754.00'),
(9, '2016-01-02', 1, 4, '2015-10-25', '2015-10-25', '2015-11-09', '100.00', 1, '500.00'),
(9, '2016-01-09', 2, 2, '2015-10-25', '2015-11-09', NULL, NULL, 1, '500.00'),
(9, '2016-01-23', 1, 4, '2015-10-25', '2015-10-24', '2015-10-25', '100.00', 1, '500.00'),
(9, '2016-06-18', 1, 2, '2016-05-25', '2016-05-25', NULL, NULL, 1, '210.00'),
(9, '2016-06-25', 1, 1, '2016-05-22', NULL, NULL, NULL, 1, '210.00'),
(10, '2015-12-26', 2, 4, '2015-10-25', '2015-10-25', '2015-10-25', '150.00', 3, '750.00'),
(10, '2016-01-30', 1, 4, '2015-07-25', '2015-07-25', '2015-07-25', '500.00', 2, '750.00'),
(10, '2016-06-18', 3, 1, '2016-07-06', NULL, NULL, NULL, 1, '206.00'),
(10, '2016-06-25', 3, 1, '2016-05-26', NULL, NULL, NULL, 1, '206.00');

-- --------------------------------------------------------

--
-- Structure de la table `saison`
--

DROP TABLE IF EXISTS `saison`;
CREATE TABLE IF NOT EXISTS `saison` (
  `CODESAISON` int(2) NOT NULL AUTO_INCREMENT,
  `NOMSAISON` char(15) COLLATE utf8_bin DEFAULT NULL,
  `DATEDEBSAISON` date DEFAULT NULL,
  `DATEFINSAISON` date DEFAULT NULL,
  PRIMARY KEY (`CODESAISON`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `saison`
--

INSERT INTO `saison` (`CODESAISON`, `NOMSAISON`, `DATEDEBSAISON`, `DATEFINSAISON`) VALUES
(2, 'Ete 2015', '2015-06-20', '2015-09-19'),
(3, 'Hiver 2015', '2015-12-19', '2016-03-19'),
(4, 'Ete 2016', '2016-06-18', '2016-09-24'),
(5, 'Hiver 2016', '2016-12-17', '2017-03-18');

-- --------------------------------------------------------

--
-- Structure de la table `semaine`
--

DROP TABLE IF EXISTS `semaine`;
CREATE TABLE IF NOT EXISTS `semaine` (
  `DATEDEBSEM` date NOT NULL,
  `CODESAISON` int(2) NOT NULL,
  `DATEFINSEM` date DEFAULT NULL,
  PRIMARY KEY (`DATEDEBSEM`),
  KEY `FK_SEMAINE_SAISON` (`CODESAISON`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `semaine`
--

INSERT INTO `semaine` (`DATEDEBSEM`, `CODESAISON`, `DATEFINSEM`) VALUES
('2015-06-20', 2, '2015-06-27'),
('2015-06-27', 2, '2015-07-04'),
('2015-07-04', 2, '2015-07-11'),
('2015-07-11', 2, '2015-07-18'),
('2015-07-18', 2, '2015-07-25'),
('2015-07-25', 2, '2015-08-01'),
('2015-08-01', 2, '2015-08-08'),
('2015-08-08', 2, '2015-08-15'),
('2015-08-15', 2, '2015-08-22'),
('2015-08-22', 2, '2015-08-29'),
('2015-08-29', 2, '2015-09-05'),
('2015-09-05', 2, '2015-09-12'),
('2015-09-12', 2, '2015-09-19'),
('2015-12-19', 3, '2015-12-26'),
('2015-12-26', 3, '2016-01-02'),
('2016-01-02', 3, '2016-01-09'),
('2016-01-09', 3, '2016-01-16'),
('2016-01-16', 3, '2016-01-23'),
('2016-01-23', 3, '2016-01-30'),
('2016-01-30', 3, '2016-02-06'),
('2016-02-06', 3, '2016-02-13'),
('2016-02-13', 3, '2016-02-20'),
('2016-02-20', 3, '2016-02-27'),
('2016-02-27', 3, '2016-03-05'),
('2016-03-05', 3, '2016-03-12'),
('2016-03-12', 3, '2016-03-19'),
('2016-06-18', 4, '2016-06-25'),
('2016-06-25', 4, '2016-07-02'),
('2016-07-02', 4, '2016-07-09'),
('2016-07-09', 4, '2016-07-16'),
('2016-07-16', 4, '2016-07-23'),
('2016-07-23', 4, '2016-07-30'),
('2016-07-30', 4, '2016-08-06'),
('2016-08-06', 4, '2016-08-13'),
('2016-08-13', 4, '2016-08-20'),
('2016-08-20', 4, '2016-08-27'),
('2016-08-27', 4, '2016-09-03'),
('2016-09-03', 4, '2016-09-10'),
('2016-09-10', 4, '2016-09-17'),
('2016-09-17', 4, '2016-09-24'),
('2016-12-17', 5, '2016-12-24'),
('2016-12-24', 5, '2016-12-31'),
('2016-12-31', 5, '2017-01-07'),
('2017-01-07', 5, '2017-01-14'),
('2017-01-14', 5, '2017-01-21'),
('2017-01-21', 5, '2017-01-28'),
('2017-01-28', 5, '2017-02-04'),
('2017-02-04', 5, '2017-02-11'),
('2017-02-11', 5, '2017-02-18'),
('2017-02-18', 5, '2017-02-25'),
('2017-02-25', 5, '2017-03-04'),
('2017-03-04', 5, '2017-03-11'),
('2017-03-11', 5, '2017-03-18');

-- --------------------------------------------------------

--
-- Structure de la table `tarif`
--

DROP TABLE IF EXISTS `tarif`;
CREATE TABLE IF NOT EXISTS `tarif` (
  `NOHEB` int(4) NOT NULL,
  `CODESAISON` int(2) NOT NULL,
  `PRIXHEB` decimal(7,2) DEFAULT NULL,
  PRIMARY KEY (`NOHEB`,`CODESAISON`),
  KEY `FK_TARIF_SAISON` (`CODESAISON`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `tarif`
--

INSERT INTO `tarif` (`NOHEB`, `CODESAISON`, `PRIXHEB`) VALUES
(2, 4, '250.00'),
(2, 5, '300.00'),
(3, 4, '350.00'),
(3, 5, '450.00'),
(4, 4, '400.00'),
(4, 5, '450.00'),
(5, 4, '754.00'),
(6, 4, '540.00'),
(6, 5, '200.00'),
(7, 4, '500.00'),
(7, 5, '750.00'),
(8, 4, '250.00'),
(8, 5, '3000.00'),
(9, 3, '500.00'),
(9, 4, '210.00'),
(9, 5, '213.00'),
(10, 3, '750.00'),
(10, 4, '206.00'),
(10, 5, '250.00');

-- --------------------------------------------------------

--
-- Structure de la table `type_heb`
--

DROP TABLE IF EXISTS `type_heb`;
CREATE TABLE IF NOT EXISTS `type_heb` (
  `CODETYPEHEB` int(5) NOT NULL AUTO_INCREMENT,
  `NOMTYPEHEB` char(30) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`CODETYPEHEB`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `type_heb`
--

INSERT INTO `type_heb` (`CODETYPEHEB`, `NOMTYPEHEB`) VALUES
(1, 'Appartement'),
(2, 'Chalet'),
(3, 'Bungalow');

-- --------------------------------------------------------

--
-- Structure de la table `villageois`
--

DROP TABLE IF EXISTS `villageois`;
CREATE TABLE IF NOT EXISTS `villageois` (
  `NOVILLAGEOIS` int(5) NOT NULL AUTO_INCREMENT,
  `USER` char(8) COLLATE utf8_bin NOT NULL,
  `NOMVILLAGEOIS` char(40) COLLATE utf8_bin DEFAULT NULL,
  `PRENOMVILLAGEOIS` char(30) COLLATE utf8_bin DEFAULT NULL,
  `ADRMAIL` char(50) COLLATE utf8_bin DEFAULT NULL,
  `NOTEL` char(15) COLLATE utf8_bin DEFAULT NULL,
  `NOPORT` char(15) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`NOVILLAGEOIS`),
  KEY `FK_VILLAGEOIS_COMPTE` (`USER`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `villageois`
--

INSERT INTO `villageois` (`NOVILLAGEOIS`, `USER`, `NOMVILLAGEOIS`, `PRENOMVILLAGEOIS`, `ADRMAIL`, `NOTEL`, `NOPORT`) VALUES
(1, 'user1', 'user1', 'user1', 'user1@mail.com', '0144556677', '0644556677'),
(2, 'user2', 'user2', 'user2', 'user2@mail.com', '0144556677', '0644556677'),
(3, 'user3', 'user3', 'user3', 'user3@gmail.com', '0144556677', '0644556677'),
(4, 'user4', 'user4', 'user4', 'user4@mail.com', '', '');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `hebergement`
--
ALTER TABLE `hebergement`
  ADD CONSTRAINT `hebergement_ibfk_1` FOREIGN KEY (`CODETYPEHEB`) REFERENCES `type_heb` (`CODETYPEHEB`);

--
-- Contraintes pour la table `resa`
--
ALTER TABLE `resa`
  ADD CONSTRAINT `resa_ibfk_2` FOREIGN KEY (`DATEDEBSEM`) REFERENCES `semaine` (`DATEDEBSEM`),
  ADD CONSTRAINT `resa_ibfk_5` FOREIGN KEY (`NOHEB`) REFERENCES `hebergement` (`NOHEB`),
  ADD CONSTRAINT `resa_ibfk_6` FOREIGN KEY (`NOVILLAGEOIS`) REFERENCES `villageois` (`NOVILLAGEOIS`),
  ADD CONSTRAINT `resa_ibfk_7` FOREIGN KEY (`CODEETATRESA`) REFERENCES `etat_resa` (`CODEETATRESA`);

--
-- Contraintes pour la table `semaine`
--
ALTER TABLE `semaine`
  ADD CONSTRAINT `semaine_ibfk_1` FOREIGN KEY (`CODESAISON`) REFERENCES `saison` (`CODESAISON`);

--
-- Contraintes pour la table `tarif`
--
ALTER TABLE `tarif`
  ADD CONSTRAINT `tarif_ibfk_3` FOREIGN KEY (`NOHEB`) REFERENCES `hebergement` (`NOHEB`),
  ADD CONSTRAINT `tarif_ibfk_4` FOREIGN KEY (`CODESAISON`) REFERENCES `saison` (`CODESAISON`);

--
-- Contraintes pour la table `villageois`
--
ALTER TABLE `villageois`
  ADD CONSTRAINT `villageois_ibfk_1` FOREIGN KEY (`USER`) REFERENCES `compte` (`USER`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;