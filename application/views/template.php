<?php
defined('BASEPATH') or exit('No direct script access allowed');

?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <!-- titre -->
        <title>VVA - <?=$titre;?></title>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="<?=$description;?>">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" href="<?=img_url('favicon.ico')?>" />

        <!-- css -->
        <?=put_headers_css();?>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- favicon -->

    </head>
    <body>
        <!-- Nav -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?=base_url();?>">VVA</a>
                </div>
                <div class="collapse navbar-collapse" id="navbar">
                    <ul class="nav navbar-nav navbar-right" >
                        <?=$menu;?>
                     </ul>
                </div>
            </div>
        </nav>

            <!-- End of visible on desktop-->
                <!-- Contenu-->
            <?=$contenu;?>
            <br/>
            <br/>
        <footer class="footer">
            <div class="container">
                <hr>
                <div class="row">
                    <div class="text-center">

                    <p><b> Technologies</b> : CodeIgniter 3 ~ PHP 5.6.16  ~ MySQL 5.7.9 ~ jQuery 1.12.0 ~ jQueryUI 1.14.4 ~ Twitter Bootstrap 3.3.6 ~ HTML 5 ~ CSS 3<br />
                        <br />
                        Louis Girones ~ BTS SIO SLAM ~ Lycée Parc de Vilgénis ~ 2016 </p>

                    </div>
                </div>
            </div>
        </footer>
        <script type="text/javascript">
        var config = {
        base: "<?php echo base_url(); ?>",

        };
        var csfrData = {};
        csfrData['<?php echo $this->security->get_csrf_token_name(); ?>']
                       = '<?php echo $this->security->get_csrf_hash(); ?>';
        </script>
        <!-- js -->
        <?=put_headers_js();?>

    </body>
</html>