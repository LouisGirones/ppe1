<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"> Ajouter une saison
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?=base_url();?>">Accueil</a></li>
                <li>Ajouter une saison</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-offset-2 col-lg-6">
            <p>Nom de la saison au format nom saison YYYY (ex: Ete 2016). <br> Les semaines de la saison seront enregistrées
              <br> <b> Une saison doit aller d'un samedi  un samedi.</b></p>
        </div>
    </div>

    <?php $attributes = ["id" => "ajout_saison_form", "name" => "ajout_saison_form"];?>
    <?=form_open("admin/ajout_saison", $attributes);?>

    <div class="row">
        <div class="col-md-offset-2 col-md-6">
            <div class="form-group">
                <label for="txt_nom_saison">Nom saison:</label>
                <input type="text" class="form-control" name="txt_nom_saison" id="txt_nom_saison" placeholder="Nom année"  value ="<?=set_value('txt_nom_saison');?>" >
                <span class="text-danger"><?=form_error('txt_nom_saison');?></span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-offset-2 col-md-3">
            <div class="form-group">
                <label for="dt_deb_saison">Date de début:</label>
                <div class='input-group'>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    <input type="text" class="form-control datePickerSamedi" name="dt_deb_saison" id="dt_deb_saison" placeholder="dd-mm-YYYY"  value ="<?=set_value('dt_deb_saison');?>" >
                </div>
                <span class="text-danger"><?=form_error('dt_deb_saison');?></span>
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label for="dt_fin">Date de fin:</label>
                <div class='input-group' >
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    <input type="text" class="form-control datePickerSamedi" name="dt_fin" id="dt_fin" placeholder="dd-mm-YYYY"  value ="<?=set_value('dt_fin');?>" >
                </div>
                <span class="text-danger"><?=form_error('dt_fin');?></span>
            </div>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-offset-4 col-md-1">
            <button type="submit" class="btn btn-primary">Ajouter la saison</button>
        </div>
    </div>
    <?=form_close();?>
    <br />
    <div class="row">
        <div class="col-lg-offset-2 col-lg-6">
            <?=$this->session->flashdata('msg');?>
        </div>
    </div>
</div>
