<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"> Ajouter un compte
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?=base_url();?>">Accueil</a></li>
                <li>Ajouter un compte</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-offset-2 col-lg-6">
            <p>Le mot de passe du compte correspond au nom d'utilisateur.</p>
        </div>
    </div>

    <?php $attributes = ["id" => "ajoutmembreform", "name" => "ajoutmembreform"];?>
    <?=form_open("admin/ajout_compte", $attributes)?>
        <div class="row">
            <!-- Colonne 1 -->
            <div class="col-md-offset-2 col-md-3">
                <div class="form-group">
                    <label class="radio-inline"><input type="radio" name="type_compte" value="VIL" id="rbVil" <?=set_radio('type_compte', 'VIL')?>>Villageois</label>
                    <label class="radio-inline"><input type="radio" name="type_compte" value="GES" id="rbGest" <?=set_radio('type_compte', 'GES')?>>Gestionnaire</label>
                    <span class="text-danger"><?=form_error('type_compte')?></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-2 col-md-3">
                <div class="form-group">
                    <label for="txt_nom">Nom:</label>
                    <input type="text" class="form-control" name="txt_nom" id="txt_nom" placeholder="Nom" value="<?=set_value('txt_nom');?>" />
                    <span class="text-danger"><?=form_error('txt_nom');?></span>
                </div>
            </div>
            <div class=" col-md-3">
                <div class="form-group">
                    <label for="txt_prenom">Prenom:</label>
                    <input type="text" class="form-control" name="txt_prenom" id="txt_prenom" placeholder="Prénom" value="<?=set_value('txt_prenom');?>" />
                    <span class="text-danger"><?=form_error('txt_prenom');?></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-2 col-md-6">
                <div class="form-group">
                    <label for="txt_login">Identifiant:</label>
                    <input type="text" class="form-control" name="txt_login" id="txt_login" placeholder="Identifiant" value="<?=set_value('txt_login');?>" />
                    <span class="text-danger"><?=form_error('txt_login');?></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-2 col-md-6">
                <div class="form-group">
                    <label for="txt_mail">Mail:</label>
                    <input type="text" class="form-control" name="txt_mail" id="txt_mail" placeholder="Mail" value="<?=set_value('txt_mail');?>" />
                    <span class="text-danger vil-err"><?=form_error('txt_mail');?></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-2 col-md-3">
                <div class="form-group">
                    <label for="txt_tel_fixe">Téléphone fixe :</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="glyphicon glyphicon-phone-alt"></i>
                        </span>
                        <input type="txt_tel_fixe" class="form-control" name="txt_tel_fixe" id="txt_tel_fixe" placeholder="Téléphone fixe" value="<?=set_value('txt_tel_fixe');?>" />
                        <span class="text-danger vil-err"><?=form_error('txt_tel_fixe');?></span>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="txt_tel_port">Téléphone portable :</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="glyphicon glyphicon-earphone"></i>
                        </span>
                        <input type="txt_tel_port" class="form-control" name="txt_tel_port" id="txt_tel_port" placeholder="Téléphone portable" value="<?=set_value('txt_tel_port');?>" />
                        <span class="text-danger vil-err"><?=form_error('txt_tel_port');?></span>
                    </div>
                </div>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-offset-4 col-md-1">
                <button type="submit" class="btn btn-primary">Ajouter le compte</button>
            </div>
        </div>
    <?=form_close();?>
    <br />
    <div class="row">
        <div class="col-lg-offset-2 col-lg-6">
            <?=$this->session->flashdata('msg');?>
        </div>
    </div>
</div>