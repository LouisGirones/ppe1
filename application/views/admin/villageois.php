<!-- Ajout fait lors de l'épreuve E4 -->
<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">
             <h1 class="page-header">
                Administration de la base de données
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?=base_url();?>">Accueil</a></li>
                <li>Gestion délais des villageois</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
        <p>Trier par année ou date début et fin.</p>
        <?php $attributes = ["id" => "reservation_form", "name" => "reservation_form", "class" => "form-inline"];?>
        <?=form_open("admin/villageois/", $attributes);?>
                <div class="form-group">
                    <label for="txt_semaine_deb">Année</label>
                    <input type='text' class="form-control" name="txt_annee" id='txt_annee' placeholder="YYYY" value="<?=set_value('txt_annee');?>"/>
                    <span class="text-danger"><?=form_error('txt_annee');?></span>
                </div>
                <div class="form-group">
                    <label for="txt_semaine_deb">Date de début</label>
                    <input type='text' class="form-control datePickerSamedi" name="txt_semaine_deb" id='txt_semaine_deb' placeholder="YYYY" value="<?=set_value('txt_semaine_deb');?>"/>
                    <span class="text-danger"><?=form_error('txt_semaine_deb');?></span>
                </div>
                <div class="form-group">
                    <label for="txt_semaine_fin">Date de fin</label>
                    <input type='text' class="form-control datePickerSamedi" name="txt_semaine_fin" id='txt_semaine_fin' placeholder="YYYY" value="<?=set_value('txt_semaine_fin');?>"/>
                    <span class="text-danger"><?=form_error('txt_semaine_fin');?></span>
                </div>
                <button type="submit" class="btn btn-default">Réserver</button>
            <?=form_close();?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">

            <div id="myTabContent" class="tab-content">
                    <h4>Compte</h4>
                    <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>Utilisateur</th>
                            <th>Nom</th>
                            <th>Prénom</th>
                            <th>Mail</th>
                            <th>Tel. fixe</th>
                            <th>Tel. portable</th>
                            <th>Nb. résas</th>
                            <th>Délais moy.</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($comptes as $compte): ?>
                            <tr>
                            <td><?=$compte->user;?></td>
                            <td><?=$compte->nomvillageois;?></td>
                            <td><?=$compte->prenomvillageois;?></td>
                            <td><?=$compte->adrmail;?></td>
                            <td><?=$compte->notel;?></td>
                            <td><?=$compte->noport;?></td>
                            <td><?=$compte->nb_resa;?></td>
                            <td><?=round($compte->delai_achat_moyen);?></td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>

        </div>
    </div>
</div>

