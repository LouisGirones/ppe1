<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">
             <h1 class="page-header">
                Administration de la base de données
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?=base_url();?>">Accueil</a></li>
                <li>Administration de la base de données</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">

            <ul id="myTab" class="nav nav-tabs nav-justified">
                <li class="active"><a href="#service-one" data-toggle="tab"><i class="glyphicon glyphicon-user"></i>Compte</a>
                </li>
                <li class=""><a href="#service-five" data-toggle="tab"><i class="glyphicon glyphicon-leaf"></i>Saisons</a>
                </li>
            </ul>

            <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade active in" id="service-one">
                    <h4>Compte</h4>
                    <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>Utilisateur</th>
                            <th>Nom</th>
                            <th>Prénom</th>
                            <th>Compte</th>
                            <th>Date d'inscription</th>
                            <th>Mail</th>
                            <th>Tel. fixe</th>
                            <th>Tel. portable</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($comptes as $compte): ?>
                            <tr>
                            <td><?=$compte->user;?></td>
                            <td><?=$compte->nomcompte;?></td>
                            <td><?=$compte->prenomcompte;?></td>
                            <td><?=$compte->typecompte;?></td>
                            <td><?=$compte->dateinscrip;?></td>
                            <td><?=$compte->adrmail;?></td>
                            <td><?=$compte->notel;?></td>
                            <td><?=$compte->noport;?></td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="service-five">
                    <h4>Saisons</h4>
                    <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>Nom saison</th>
                            <th>Date de début</th>
                            <th>Date de fin</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($saisons as $saison): ?>
                            <tr>
                            <td><?=$saison->nomsaison;?></td>
                            <td><?=$saison->datedebsaison;?></td>
                            <td><?=$saison->datefinsaison;?></td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>

