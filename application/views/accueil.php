<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">
             <h1 class="page-header">
                La présentation de Village Vacances Alpes
            </h1>
            <ol class="breadcrumb">
                <li>Accueil</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <br>
          <p>L’association VVA (Village Vacances Alpes) gère, en relation avec les collectivités territoriales propriétaires de villages de vacances,
          les sites qui lui sont confiés. Ces sites sont essentiellement situés dans les Alpes en haute, moyenne et basse montagne.
          Son développement se poursuit et VVA commence à développer son activité sur les autres massifs français : les Pyrénées, les Vosges, le Jura et le Massif Central.
          L'association s'assure des bonnes pratiques des uns et des autres villages et travaille sur les moyens à mettre en œuvre pour renforcer la pertinence des
          villages dans l’offre touristique du territoire de montagne.
          <br>
          L'association VVA est également partenaire des labels « Famille Plus » et « Station Verte » ainsi que de
          « L’Association Nationale des Maires des Stations Classées et des Communes Touristiques de montagne».
          <br>
          L'association VVA propose un tourisme plus humain, plus authentique, plus respectueux de la planète.
          Cette responsabilité sociale, économique et écologique est dans l'ADN de la marque. Elle vient d'être traduite dans une charte d'engagements.
          <br>
          L'objectif de l'association est de constituer dans chaque village une équipe compétente et accueillante qui permette de passer des vacances en toute sérénité,
             d'être attentif aux demandes des villageois dès le premier contact, puis tout au long du séjour et de garantir un accueil chaleureux et personnalisé.
          L'association VVA propose d'offrir au quotidien et à tous les niveaux une excellente qualité de service,
          d'offrir des moments de rencontre et d’échange entre villageois et de mesurer chaque année la satisfaction des villageois pour améliorer ses prestations.
          <br>
          Les responsables de VVA ont également découvert de nouveaux produits phares mis en place progressivement en France, des nouveautés en matière de restauration
          et de défis sportifs. Le patrimoine de Pelvoux et de Vallouise, où sont implantés deux villages à la gestion commune,
          font figure de modèles et le site de Pelvoux est déclaré site pilote de l'association.</p>
      </div>
   </div>
</div>
