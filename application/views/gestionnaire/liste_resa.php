<div class="container">
    <!-- Page Heading/Breadcrumbs -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Liste des réservations
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?=base_url();?>">Accueil</a></li>
                <li>Liste des réservations</li>
            </ol>
        </div>
    </div>
    <!-- Content Row -->
    <div class="row">
        <div class="col-lg-9">
            <?=$this->session->flashdata('msg');?>
            <?php
$compteur = 0;
foreach ($resas as $r):
    if ($r->montantarrhes == null) {
        $r->montantarrhes = "/";
    }
    if ($r->datearrhes == null) {
        $r->datearrhes = "/";
    }
    if ($r->dateaccuserecept == null) {
        $r->dateaccuserecept = "/";
    }
    $id = $r->noheb . '_' . $r->datedebsem;
    ?>
	            <!-- ancre -->
	            <?php
    $page = $this->uri->segment(3) > 0 ? '/' . $this->uri->segment(3) : '';
    $etat = !empty($_GET['etatRecherche']) ? '?etatRecherche=' . (int) $_GET['etatRecherche'] : '';
    $semaine = !empty($_GET['semaineRecherche']) ? '?semaineRecherche=' . $_GET['semaineRecherche'] : '';
    ?>
	            <?php $attributes = array( "id" => "reservation_form", "name" => "reservation_form", "class"=>"form-inline");?>
	            <?=form_open("gestionnaire/index$page$etat$semaine#$r->noheb" . "$r->datedebsem", $attributes)?>
	            <div id="<?=$r->noheb . $r->datedebsem;?>" class="ancre-resa"></div>
	                <table class="table table-bordered  table-responsive <?=($compteur % 2 == 0) ? '' : 'table-color';?> ">
	                    <tbody>
	                        <tr>
	                            <!-- Informations générales -->
	                            <td>N° d'hébergement : <?=$r->noheb;?></td>
	                            <td>Semaine de début :<br> <?=$r->datedebsem;?></td>
	                            <td>N° de villageois : <?=$r->novillageois;?></td>
	                            <td>Date de la réservation : <br> <?=$r->dateresa;?></td>
	                            <td>Nb occupants : <?=$r->nboccupant;?></td>
	                            <td>Prix : <?=$r->prixresa;?></td>
	                        </tr>
	                        <tr>
	                            <!-- Informations a completer -->
	                            <td colspan="2">Dates accusée de recept : <?=$r->dateaccuserecept;?>
	                                <?php if ($r->dateaccuserecept == "/"): ?>
	                                <br>
	                                <!-- Date accusée de reception -->
	                                <div class="form-group">
	                                    <div class='input-group' >
	                                        <span class="input-group-addon">
	                                        <span class="glyphicon glyphicon-calendar"></span>
	                                        </span>
	                                        <input type='text' class="form-control datePicker" name="txt_date_acc_recept_<?=$id;?>" value="<?=set_value('txt_date_acc_recept_' . $id);?>" placeholder="dd-mm-yyyy"/>
	                                    </div>
	                                    <span class="text-danger"><?=form_error('txt_date_acc_recept_' . $id)?></span>
	                                </div>

	                                <?php endif;?>
                            </td>
                            <td colspan="2">Dates réception arrhes : <?=$r->datearrhes;?>
                                <?php if ($r->datearrhes == "/"): ?>
                                <br>
                                <!-- Date reception arrhes -->
                                <div class="form-group">
                                    <div class='input-group' >
                                        <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                        <input type='text' <?php if ($r->codeetatresa < 2) {echo "disabled";}?> class="form-control datePicker" name="txt_date_arrhes_<?=$id;?>" value="<?=set_value('txt_date_arrhes_' . $id);?>" placeholder="dd-mm-yyyy"/>
                                    </div>
                                    <span class="text-danger"><?=form_error('txt_date_arrhes_' . $id);?></span>
                                </div>

                                <?php endif;?>
                            </td>
                            <td colspan="2">Montant arrhes reçu :<small>(<?=$r->prixresa * 0.2;?>€ attendu)</small> : <?=$r->montantarrhes;?>
                                <?php if ($r->montantarrhes == "/"): ?>
                                <br>
                                <!-- Date reception arrhes -->
                                <div class="form-group">
                                    <div class='input-group' >
                                        <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-euro"></span>
                                        </span>
                                        <input type="number" <?php if ($r->codeetatresa < 2) {echo "disabled";}?> value="<?=set_value('txt_mt_arrhes_' . $id);?>" class="form-control" step="any" name="txt_mt_arrhes_<?=$id;?>" min="1" max="500" >
                                    </div>
                                    <span class="text-danger"><?=form_error('txt_mt_arrhes_' . $id);?></span>
                                </div>

                                <?php endif;?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">Etat de la réservation : <?=$r->nometatresa . " " . "(" . $r->codeetatresa . "/4)";?><br>
                                <?php if ($r->codeetatresa < 4): ?>
                                    <select name="nv_etat_resa_<?=$id;?>" class="form-control">
                                        <?php foreach ($etats_resa as $etat): ?>
                                            <?php if ($etat->codeetatresa == $r->codeetatresa + 1): ?>
                                            <option value="<?=$etat->codeetatresa;?>"><?=$etat->nometatresa;?></option>
                                            <?php endif;?>
                                        <?php endforeach;?>
                                    </select>
                                <?php endif;?>
                            </td>

                            <td colspan="3">Jours restants arrhes :
                            <?php
if ($r->datearrhes == "/" || $r->montantarrhes == "/") {
    echo $r->jours_restant_arrhes;
    //Délais pour payer les arrhes écoulés.
    //On ne peut que supprimer la réservation
    if ($r->jours_restant_arrhes <= 0) {
        echo anchor("/gestionnaire/supprimer_resa/" . $r->noheb . "/" . $r->datedebsem, '<button type="button" class="btn btn-danger btn-xs">Supprimer la réservation.</button>', ['onclick' => "return confirm('Etes-vous sur de vouloir supprimer cette réservation?')"]);
    }
} else {
    $r->jours_restant_arrhes = 1;
    echo "arrhes payés.";
    echo anchor("/gestionnaire/supprimer_resa/" . $r->noheb . "/" . $r->datedebsem, '<button type="button" class="btn btn-danger btn-xs">Supprimer la réservation.</button>', ['onclick' => "return confirm('Etes-vous sur de vouloir supprimer cette réservation?')"]);
}
?>
                            </td>
                        </tr>
                            <?php if (($r->codeetatresa < 4 || $r->datearrhes == "/" || $r->montantarrhes == "/" || $r->dateaccuserecept == "/") && $r->jours_restant_arrhes > 0): ?>
                            <tr>
                                <td colspan="6">
                                <input type="hidden" name ="etat_resa" value="<?=$r->codeetatresa;?>">
                                <input type="hidden" name="dt_deb_sem" value="<?=$r->datedebsem;?>">
                                <input type="hidden" name="no_heb" value="<?=$r->noheb;?>">
                                <input type="submit" class="btn btn-primary center" name="modif_etat_resa" value="Modifier l'état de la réservation">
                                <span class="text-danger"><?=form_error('dt_deb_sem');?></span>
                               </td>
                            </tr>
                            <?php endif;?>
                    </tbody>
                </table>
            <?=form_close();?>
            <?=$this->session->flashdata('msg_' . $id);?>
            <hr>
        <?php $compteur++;?>
        <?php endforeach;?>
    </div>
        <!-- Blog Sidebar Widgets Column -->
        <div class="col-md-3">
            <!-- Side Widget Well -->
            <div class="well">
            <?php if ($this->router->fetch_method() != 'historique_reservations'): ?>
                <h4>Recherche :</h4>
                <hr class="dark-line">
                <?php $attributes = ["id" => "recherche_etat_form", "name" => "recherche_etat_form", "method" => "get"];?>
                <?=form_open("/gestionnaire/index", $attributes)?>
                <label for="etatRecherche">Etat :</label>
                <select name="etatRecherche" id="etatRecherche" class="form-control">
                    <?php foreach ($etats_resa as $etat): ?>
                        <option value="<?=$etat->codeetatresa;?>" <?=set_select('etatRecherche', $etat->codeetatresa);?>><?=$etat->nometatresa;?></option>
                    <?php endforeach;?>
                </select>
                <span class="text-danger"><?=form_error('etatRechercheetatRecherche');?></span>
                <button type="submit" class="btn btn-primary center bt-recherche">Rechercher</button>
                <?=form_close();?>
                <hr class="dark-line">
                <?php $attributes = ["id" => "recherche_semaine_form", "name" => "recherche_semaine_form", "method" => "get"];?>
                <?=form_open("/gestionnaire/index", $attributes);?>
                <label for="semaineRecherche">Semaine de début :</label>
                <input type="text" name="semaineRecherche" id="semaineRecherche" class="form-control datePickerSamedi">
                <span class="text-danger"><?=form_error('semaineRecherche');?></span>
                <button type="submit" class="btn btn-primary center bt-recherche">Rechercher</button>
                <?=form_close();?>

                <hr class="dark-line">

                <a href="<?=site_url('gestionnaire/historique_reservations');?>"><button type="button" class="btn btn-primary center bt-recherche">Historique</button> </a>
            <?php else: ?>
                <a href="<?=site_url('gestionnaire/');?>"><button type="button" class="btn btn-primary center bt-recherche">Réservations</button> </a>
            <?php endif;?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="text-center">
            <nav>
                <ul class="pagination">
                    <?php
if (!empty($chaine)) {
    $links = preg_replace("/(gestionnaire\/index\/\d)/", "$1$chaine", $links);
}
?>
                    <?=$links;?>
                </ul>
            </nav>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container -->
