<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
            <?=$titre?>

            </h1>
                <ol class="breadcrumb">
                        <li><a href="<?=base_url();?>">Accueil</a></li>
                        <li><?=$titre?></a></li>
                </ol>
        </div>
    </div>
    <?php
        if ($modif == true) {
            $attributes = ["id" => "modifier_heberg", "name" => "modifier_heberg"];
            echo form_open_multipart("hebergement/update_hebergement/true/$heberg->noheb", $attributes);
        } else {
            $attributes = ["id" => "ajouter_heberg", "name" => "ajouter_heberg"];
            echo form_open_multipart("hebergement/update_hebergement", $attributes);
        }
    ?>
        <div class="row">
            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="txt_nom">Nom :</label>
                    <input type="text" class="form-control" name="txt_nom" id="txt_nom" placeholder="Nom" value ="<?=set_value('txt_nom', !empty($heberg->nomheb) ? $heberg->nomheb : '');?>" >
                    <span class="text-danger"><?=form_error('txt_nom');?></span>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <label for="txt_nb_place">Place:</label>
                    <input type="number" class="form-control" name="txt_nb_place" id="txt_nb_place" min="1" max="10" value="<?=set_value('txt_nb_place', !empty($heberg->nbplaceheb) ? $heberg->nbplaceheb : '');?>">
                    <span class="text-danger"><?=form_error('txt_nb_place');?></span>
                </div>
            </div>
             <div class="col-md-1">
                <div class="form-group">
                    <label for="txt_surface">Surface:</label>
                    <input type="number" class="form-control" name="txt_surface" id="txt_surface" min="15" max="199" value="<?=set_value('txt_surface', !empty($heberg->surfaceheb) ? $heberg->surfaceheb : '')?>">
                    <span class="text-danger"><?=form_error('txt_surface')?></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-2 col-md-2">
                <div class="form-group">
                    <label for="typeHeb">Type d'hébergement :</label>
                    <select name="txt_type_heb" class="form-control" id="txt_type_heb">
                        <?php foreach ($types_heberg as $type): ?>
                            <option
                            value="
                                <?=$type->codetypeheb;?>" <?=set_select('txt_type_heb', $type->codetypeheb, !empty($heberg->codetypeheb) ? ($heberg->codetypeheb == $type->codetypeheb ? true : false) : '')?> ><?=$type->nomtypeheb?></option>
                        <?php endforeach;?>
                    <select>
                    <span class="text-danger"><?=form_error('txt_type_heb');?></span>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="txt_internet">Internet :</label>
                    <select name="txt_internet" class="form-control" id="txt_internet">
                        <option value="1" <?=set_select('txt_internet', 1, !empty($heberg->internet) ? ($heberg->internet == 1 ? true : false) : '')?> >Oui</option>
                        <option value="0" <?=set_select('txt_internet', 0, !empty($heberg->internet) ? ($heberg->internet == 0 ? true : false) : '')?> >Non</option>
                    <select>
                    <span class="text-danger"><?=form_error('txt_internet');?></span>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="annee">Annee rénovation:</label>
                    <select name="txt_annee" class="form-control" id="txt_annee">
                        <?php for ($annee = date('Y') - 5; $annee <= date('Y'); $annee++): ?>
                            <option value="<?=$annee;?>" <?=set_select('txt_annee', $annee, !empty($heberg->anneeheb) ? ($heberg->anneeheb == $annee ? true : false) : '')?> > <?=$annee;?> </option>
                        <?php endfor;?>
                    <select>
                    <span class="text-danger"><?=form_error('txt_annee');?></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-2 col-md-3">
                <div class="form-group">
                    <label for="txt_etat">Etat :</label>
                    <select name="txt_etat" class="form-control" id="txt_etat">
                        <?php foreach ($etat as $e): ?>
                            <option value="<?=$e;?>" <?=set_select('txt_etat', $e, !empty($heberg->etatheb) ? ($heberg->etatheb == $e ? true : false) : '')?> > <?=$e;?> </option>
                        <?php endforeach;?>
                    <select>
                    <span class="text-danger"><?=form_error('txt_etat');?></span>
                </div>
            </div>
                        <div class="col-md-3">
                <div class="form-group">
                    <label for="txt_secteur">Secteur :</label>
                    <select name="txt_secteur" class="form-control" id="txt_secteur">
                        <?php foreach ($secteur as $s): ?>
                            <option value="<?=$s;?>" <?=set_select('txt_secteur', $s, !empty($heberg->secteurheb) ? ($heberg->secteurheb == $s ? true : false) : '')?> > <?=$s;?> </option>
                        <?php endforeach;?>
                    <select>
                    <span class="text-danger"><?=form_error('txt_secteur');?></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-2 col-md-3">
                <div class="form-group">
                    <label for="txt_orientation">Orientation :</label>
                    <select name="txt_orientation" class="form-control" id="txt_orientation">
                        <?php foreach ($orientation as $o): ?>
                            <option value="<?=$o;?>" <?=set_select('txt_orientation', $o, !empty($heberg->orientationheb) ? ($heberg->orientationheb == $o ? true : false) : '')?> > <?=$o?> </option>
                        <?php endforeach;?>
                    <select>
                    <span class="text-danger"><?=form_error('txt_orientation');?></span>
                </div>
            </div>
        <div class="col-md-3">
                <div class="form-group">
                        <label for="photo" class="control-label">Photo :</label>
                        <input type="file" name="photo" id="photo" size="20">
                        <span class="text-danger"><?php if (isset($error_upload)) {echo $error_upload;}?></span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-2 col-md-6">
                <div class="form-group">
                    <label for="txt_description">Description :</label>
                    <textarea class="form-control" rows="5" name="txt_description" id="txt_description">
                    <?=set_value('txt_description', !empty($heberg->descriheb) ? $heberg->descriheb : '')?>
                    </textarea>
                    <span class="text-danger"><?=form_error('txt_description');?></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-4 col-md-1">
                <input type="hidden" name ="verif">
                <button type="submit" class="btn btn-primary"><?= ($modif ? 'Modifier l\'hébergement' : 'Ajouter un hébergement') ?></button>
            </div>
        </div>
    <?=form_close()?>
</div>