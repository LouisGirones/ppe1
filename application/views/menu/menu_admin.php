<li class="<?=active_link('accueil');?>">
    <a href="<?=base_url();?>">Accueil</a>
</li>
<li class="<?=active_link('admin', 'index');?>">
    <a href="<?=site_url('admin/');?>">Gestion base de données</a>
</li>
<li class="<?=active_link('admin', 'ajout_compte');?> <?=active_link('admin', 'ajout_saison');?>">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Ajout <span class="caret"></span></a>
    <ul class="dropdown-menu">
        <li class="<?=active_link('admin', 'ajout_compte');?>">
            <a href="<?=site_url('admin/ajout_compte');?>">Compte</a>
        </li>
        <li class="<?=active_link('admin', 'ajout_saison');?>"">
            <a href="<?=site_url('admin/ajout_saison');?>">Saison</a>
        </li>
    </ul>
</li>
<li>
  <a href="<?=site_url('/logout');?>">Déconnexion</a>
</li>
