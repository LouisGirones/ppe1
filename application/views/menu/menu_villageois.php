<li class="<?=active_link('accueil');?>">
    <a href="<?=base_url();?>">Accueil</a>
</li>
<li class="<?=active_link('engagements');?>">
    <a href="<?=site_url('/engagements');?>">Engagements</a>
</li>
<li class="<?=active_link('villageois');?>">
    <a href="<?=site_url("/villageois");?>">Compte</a>
</li>
<li class="<?=active_link('hebergement');?> <?=active_link('villageois', 'reserver_heberg')?>">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Hebergements<span class="caret"></span></a>
    <ul class="dropdown-menu">
        <li class="<?=active_link('hebergement', 'index');?>">
            <a href="<?=site_url("/hebergement");?>">Voir les hébergements</a>
        </li>
        <li class="<?=active_link('hebergement', 'rechercher_heberg');?>">
            <a href="<?=site_url("/hebergement/rechercher_heberg");?>">Chercher un hébergement</a>
        </li>
    </ul>
</li>
<li>
  <a href="<?=site_url('/logout');?>">Déconnexion</a>
</li>