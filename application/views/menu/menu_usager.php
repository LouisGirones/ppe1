<li class="<?=active_link('accueil');?>">
    <a href="<?=base_url();?>">Accueil</a>
</li>
<li class="<?=active_link('engagements');?>">
    <a href="<?=site_url('/engagements');?>">Engagements</a>
</li>
<li class="<?=active_link('hebergement');?>">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Hebergements<span class="caret"></span></a>
    <ul class="dropdown-menu">
        <li class="<?=active_link('hebergement', 'index');?>">
            <a href="<?=site_url("/hebergement");?>">Voir les hébergements</a>
        </li>
        <li class="<?=active_link('hebergement', 'rechercher_heberg');?>">
            <a href="<?=site_url("/hebergement/rechercher_heberg");?>">Chercher un hébergement</a>
        </li>
    </ul>
</li>
<li class="<?=active_link('connexion');?>">
    <a href="<?=site_url('/connexion');?>">Connexion</a>
</li>
