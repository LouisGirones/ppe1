<div class="container">
    <div class="row">
        <div class="col-lg-12">
             <h1 class="page-header">
                 Hébergements
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?=base_url();?>">Accueil</a></li>
                <li>Hébergements</li>
            </ol>
        </div>
    </div>
    <div class="row">
    <?php $i = 0; //compteur pour les row ?>
    <?php foreach ($hebergs as $heberg): ?>
        <div class="col-md-4">
            <a href="<?=site_url('/hebergement/voir_heberg/' . $heberg->noheb)?>" class="thumbnail">
            <?=img("uploads/imgHebergement/$heberg->nomtypeheb/$heberg->photoheb", "image $heberg->nomheb", "img-responsive img-hebergs img-hover");?>

            </a>
            <h3>
                <a href="<?=site_url('/hebergement/voir_heberg/' . $heberg->noheb)?>"><?=htmlspecialchars($heberg->nomheb);?> </a>
            </h3>
            <p><b><?=$heberg->nomtypeheb;?></b> <br>
            <?php $heberg->descriheb = $heberg->descriheb > 50 ? substr($heberg->descriheb, 0, 50) . "..." : $heberg->descriheb;?>
            <?=$heberg->descriheb;?>
            </p>

        </div>
        <?php
$i++;
if ($i % 3 == 0):
?>
        </div>
            <div class="row">
        <?php endif;?>
    <?php endforeach;?>
    </div>
    <div class="row">
        <div class="text-center">
            <nav>
                <ul class="pagination">
                    <?=$links;?>
                </ul>
            </nav>
        </div>
    </div>
</div>
