<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Chercher un hébergement
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?=base_url();?>">Accueil</a></li>
                <li>Chercher un hébergement</li>
            </ol>
        </div>
    </div>
    <div class="row">
    	<div class="col-lg-12">
			<h2> Par nom </h2>
		</div>
		<div class="col-lg-offset-3 col-lg-6 ">
        <?php $attributes = ["id" => "recherche_heberg_form", "name" => "recherche_heberg_form"];?>
            <?=form_open("hebergement/rechercher_heberg", $attributes)?>
                <div class="form-group">
                    <label for="txt_nomheb">Nom:</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="glyphicon glyphicon-home"></i>
                        </span>
                        <input type="text"
                            class="form-control"
                            name="txt_nomheb" id="txt_nomheb"
                            value="" />
                    </div>
                    <span class="text-danger"><?=form_error('txt_nomheb');?></span>
                </div>
                <div class="form-group">
                    <div class="col-lg-12 col-sm-12 text-center">
                        <input id="btn_recherche" name="btn_recherche" type="submit" class="btn btn-default" value="Recherche" />
                    </div>
                </div>
          <?=form_close()?>
          <br />
          <br />
          <?=$this->session->flashdata('msg')?>
		</div>
	</div>
</div>