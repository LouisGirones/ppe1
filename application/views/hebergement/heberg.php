<?php $heberg->internet = $heberg->internet == 1 ? "Oui" : "Non";?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                 <h1 class="page-header">
                   <?=$heberg->nomheb;?>
                </h1>
            <ol class="breadcrumb">
                <li><a href="<?=base_url();?>">Accueil</a></li>
                <li><a href="<?=site_url('hebergement');?>">Liste Hébergements</a></li>
                <li><?=$heberg->nomheb;?></li>
            </ol>
            </div>
        </div>

        <?=$this->session->flashdata('msg_heberg');?>
        <div class="thumbnail">
            <div class="row">
                <div class="col-md-4">
                    <?=img("uploads/imgHebergement/$heberg->nomtypeheb/$heberg->photoheb", "image $heberg->nomheb", "img-responsive");?>
                </div>
                <div class="col-md-3">
                    <div class="caption">
                        <h2>Infos</h2>
                        <hr>
                        <p><b> Type :</b> <?=$heberg->nomtypeheb;?> <br>
                            <b>Nombre de place :</b> <?=$heberg->nbplaceheb;?> <br>
                            <b>Surface : </b><?=$heberg->surfaceheb;?> <br>
                            <b>Internet : </b><?=$heberg->internet;?> <br>
                            <b>Année de rénovation :</b> <?=$heberg->anneeheb;?><br>
                            <b>Etat :</b> <?=$heberg->etatheb;?> <br />
                            <b>Secteur : </b><?=$heberg->secteurheb;?> <br>
                            <b>Orientation : </b><?=$heberg->orientationheb;?> <br>
                        </p>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="caption">
                        <?php if ($this->session->userdata('user_type') === "GES"): ?>
                        <h2>Tarifs :</h2>
                        <hr>
                            <?php foreach ($prix_saisons as $prix_saison): ?>
                                <?php if ($prix_saison->prixheb !== null): ?>
                                    Saison <?=$prix_saison->nomsaison;?>
                                    <small><?=$prix_saison->prixheb;?>€ / semaine.</small><br>
                                <?php else: ?>
                                        <?php $attributes = ["id" => "ajout_saison_form", "name" => "ajout_saison_form", "class" => "form-inline"];?>
                                        <?=form_open("hebergement/voir_heberg/" . $heberg->noheb, $attributes);?>
                                    <div class="form-group">
                                        <label for="<?=$prix_saison->codesaison;?>">Saison <?=$prix_saison->nomsaison;?>:</label>
                                        <input type="number" class="form-control input-sm" min="200" max="9000" name="txt_prix_saison" id="<?=$prix_saison->codesaison;?>" placeholder="Prix"  value ="<?=set_value('txt_prix_saison');?>" >
                                    </div>
                                    <input type="hidden"  name="noHeb" value="<?=$heberg->noheb?>"/>
                                    <input type="hidden"  name="cdSaison" value="<?=$prix_saison->codesaison;?>"/>
                                    <button type="submit" class="btn btn-success btn-xs">Enregistrer le tarif</button>
                                    <span class="text-danger"><?php echo form_error('txt_prix_saison'); ?></span>
                                    <?php echo form_close(); ?>
                                    <br>
                                <?php endif?>
                            <?php endforeach;?>
                            <?=$this->session->flashdata('msg_tarif');?>
                            <a href="<?=site_url('hebergement/update_hebergement/true/' . $heberg->noheb);?>">Modifier l'hébérgement</a>
                        <?php elseif ($this->session->userdata('user_type') === "VIL"): ?>
                        <h2>Réserver</h2>
                        <hr>
                            <?php foreach ($prix_saisons as $prix_saison): ?>
                                <?php if ($prix_saison->prixheb !== null): ?>
                                    Saison <?=$prix_saison->nomsaison;?></a>
                                    <small><?=$prix_saison->prixheb;?>€ / semaine.</small>
                                    <a href="<?=site_url('villageois/reserver_heberg/' . $heberg->noheb . '/' . $prix_saison->codesaison);?>">Réserver</a><br>
                                <?php else: ?>
                                    Saison <?=$prix_saison->nomsaison;?>
                                    <small>Indisponible pour cet hébergement.</small><br>
                                <?php endif?>
                            <?php endforeach;?>

                            <?php elseif ($this->session->userdata('user_type') === null): ?>
                            <p>Vous devez être connecté pour réserver un hébergement.<p><a href="<?=site_url('/connexion')?>">Se connecter</a>
                        <?php endif;?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h2> Description : </h2>
                <hr>
                <p><?=$heberg->descriheb;?> </p>
                <br>
            </div>
       </div>
    </div>

