<?php
if ($resa->internet == 1) {
    $resa->internet = "Oui";
} else {
    $resa->internet = "Non";
}
if ($resa->montantarrhes == null) {
    $resa->montantarrhes = "/";
}
if ($resa->datearrhes == null) {
    $resa->datearrhes = "/";
}
if ($resa->dateaccuserecept == null) {
    $resa->dateaccuserecept = "/";
}
?>
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                 <h1 class="page-header">
                    Facture
                </h1>
                <ol class="breadcrumb">
                    <li><a href="<?=base_url();?>">Accueil</a></li>
                    <li><a href="<?=site_url('villageois');?>">Votre compte</a></li>
                    <li>Réservation de l'hébergement <?=$resa->nomheb;?> pour la semaine du <?=$resa->datedebsem;?></li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-lg-offset-4">
                <?php echo $this->session->flashdata('msg'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <table class="table table-bordered table-responsive">
                        <tbody>
                            <tr>
                               <td colspan="6"> <h3>Récapitulatif :</h3></td>
                            </tr>
                            <tr>
                                <td>N° d'hébergement : <?=$resa->noheb;?></td>
                                <td>Semaine de début :<br> <?=$resa->datedebsem;?></td>
                                <td>N° de villageois : <?=$this->session->userdata('num_villageois');?></td>
                                <td>Date de la réservation : <br> <?=$resa->dateresa;?></td>
                                <td>Nb occupants : <?=$resa->nboccupant;?></td>
                                <td>Prix : <?=$resa->prixresa;?></td>
                            </tr>
                            <tr>
                                <td colspan="2">Dates accusee de recept :
                                    <?=$resa->dateaccuserecept;?>
                                </td>
                                <td colspan="2">Dates réception arrhes : <?=$resa->datearrhes;?>
                                </td>
                                <td colspan="2">Montant arrhes reçu :<small>(<?=$resa->prixresa * 0.2;?>€ attendu)</small> : <?=$resa->montantarrhes;?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">Etat de la réservation : <?=$resa->nometatresa;?><br>
                                </td>
                                <td colspan="2">Jours restants arrhes :
                                <?php if ($resa->datearrhes == "/" || $resa->montantarrhes == "/"): ?>
                                    <?=$resa->jours_restant_arrhes?>
                                <?php else: ?>
                                    <?="Arrhes payés."?>
                                <?php endif;?>
                                </td>
                                <td colspan="1">ID : <br> </td>
                            </tr>
                            <tr>
                               <td colspan="6"> <h3>Hebergement :</h3></td>
                            </tr>
                            <tr>
                                <td colspan="1">Nom : <?=$resa->nomheb;?></td>
                                <td colspan="1">Type : <?=$resa->nomtypeheb;?></td>
                                <td colspan="2">Nombre de place : <?=$resa->nbplaceheb;?></td>
                                <td colspan="2">Internet : <?=$resa->internet;?></td>
                            </tr>
                            <tr>

                                <td colspan="2">Dernière rénovation : <?=$resa->anneeheb;?></td>
                                <td colspan="2">Secteur : <?=$resa->secteurheb;?></td>
                                <td colspan="1">Orientation : <?=$resa->orientationheb;?></td>
                                <td colspan="1">Etat : <?=$resa->etatheb;?></td>
                            </tr>
                            <tr>
                               <td colspan="6"> <h3>Vous :</h3></td>
                            </tr>
                            <tr>
                                <td colspan="2">Compte : <?=$this->session->userdata('user_login');?></td>
                                <td colspan="1">Nom : <?=$this->session->userdata('nom_compte');?></td>
                                <td colspan="1">Prenom : <?=$this->session->userdata('prenom_compte');?></td>
                                <td colspan="2">Date d'inscription : <?=$this->session->userdata('date_inscript');?></td>
                            </tr>
                            <tr>
                                <td colspan="2">Mail : <?=$this->session->userdata('mail_villageois');?></td>
                                <td colspan="2">Tél. : <?=$this->session->userdata('tel_villageois');?></td>
                                <td colspan="2">Tél. portable : <?=$this->session->userdata('num_villageois');?></td>
                            </tr>
                        </tbody>
                    </table>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-offset-4 col-lg-2">
            <?=anchor("/villageois/supprimer_resa/" . $resa->noheb . "/" . $resa->datedebsem, '<button type="button" class="btn btn-danger">Annuler la réservation.</button>', ['onclick' => "return confirm('Etes-vous sur de vouloir supprimer cette réservation?')"])?>
            </div>
        </div>
    </div>
