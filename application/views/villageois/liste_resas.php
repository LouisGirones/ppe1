<!-- Page Content -->
<div class="container">

   	<div class="row">
        <div class="col-lg-12">
             <h1>
                Réservations
            </h1>
            <hr>
        </div>
    </div>
            <!-- Content Row -->
    <div class="row">
        <div class="col-lg-9">

            <?php $compteur = 0;?>

            <?php 
                foreach ($resas as $resa):
                    if ($resa->montantarrhes == null) {
                        $resa->montantarrhes = "/";
                    }
                    if ($resa->datearrhes == null) {
                        $resa->datearrhes = "/";
                    }
                    if ($resa->dateaccuserecept == null) {
                        $resa->dateaccuserecept = "/";
                    }
            ?>
            <form method="post" action="#">
                <table class="table table-bordered  table-responsive <?=($compteur % 2 == 0) ? '' : 'table-color';?> ">
                    <tbody>
                        <tr>
                            <td>N° d'hébergement : <a href="<?=site_url('hebergement/voir_heberg/' . $resa->noheb);?>"><?=$resa->noheb;?></a></td>
                            <td>Semaine de début :<br> <?=$resa->datedebsem;?></td>
                            <td>N° de villageois : <?=$this->session->userdata('num_villageois');?></td>
                            <td>Date de la réservation : <br> <?=$resa->dateresa;?></td>
                            <td>Nb occupants : <?=$resa->nboccupant;?></td>
                            <td>Prix : <?=$resa->prixresa;?></td>
                        </tr>
                        <tr>
                            <td colspan="2">Dates accusee de recept :
                                <?=$resa->dateaccuserecept;?>
                            </td>
                            <td colspan="2">Dates réception arrhes : <?=$resa->datearrhes;?>
                            </td>
                            <td colspan="2">Montant arrhes reçu :<small>(<?=$resa->prixresa * 0.2;?>€ attendu)</small> : <?=$resa->montantarrhes;?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">Etat de la réservation : <?=$resa->nometatresa;?><br>
                            </td>
                            <td colspan="2">Jours restants arrhes :
                            <?php if ($resa->datearrhes == "/" || $resa->montantarrhes == "/") : ?>
                                <?= $resa->jours_restant_arrhes;?>{
                            <?php else : ?>
                                <?= "Arrhes payés." ?>
                                </td>
                            <?php endif;?>
                            <td colspan="1">ID : <br> </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <a href="<?=site_url('villageois/details_resa/' . $resa->noheb . '/' . $resa->datedebsem);?>"> <button type="button" class="btn btn-primary center">Voir le détail </button></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
            <hr>
        <?php $compteur++;?>
        <?php endforeach;?>
    </div>
        <!-- Blog Sidebar Widgets Column -->
        <div class="col-md-3 well">
            <?php
                //On récupére le nom de la méthode (resas actuelles ou historique)
                $method = $this->router->fetch_method();
                $lien = $method === 'index' ? 'villageois/historique_reservations' : 'villageois/';
                $nom_bouton = $method === 'index' ? 'Historique des réservations' : 'Réservations en cours';
            ?>
            <a href="<?=site_url($lien);?>"> <button type="button" class="btn btn-primary center"><?=$nom_bouton;?></button> </a>
        </div>
    </div>
    <div class="row">
        <div class="text-center">
            <nav>
                <ul class="pagination">
                    <?=$links;?>
                </ul>
            </nav>
        </div>
    </div>
</div>
