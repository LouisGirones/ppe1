<?php $heberg->internet = $heberg->internet == 1 ? 'Oui' : 'Non';?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
             <h1 class="page-header">
                Réserver <?=$heberg->nomheb;?>
            </h1>

        </div>
    </div>
    <div class="thumbnail">
        <div class="row">
            <div class="col-md-4">
                <?=img("uploads/imgHebergement/$heberg->nomtypeheb/$heberg->photoheb", "image $heberg->nomheb", "img-responsive");?>
            </div>
            <div class="col-md-4">
                <div class="caption">
                    <h2>Infos</h2>
                    <hr>
                    <p><b> Type :</b> <?=$heberg->nomtypeheb;?> <br>
                        <b>Nombre de place :</b> <?=$heberg->nbplaceheb;?> <br>
                        <b>Surface : </b><?=$heberg->surfaceheb;?> <br>
                        <b>Internet : </b><?=$heberg->internet;?> <br>
                        <b>Année de rénovation :</b> <?=$heberg->anneeheb;?><br>
                        <b>Etat :</b> <?=$heberg->etatheb;?>
                        <b>Secteur : </b><?=$heberg->secteurheb;?> <br>
                        <b>Orientation : </b><?=$heberg->orientationheb;?> <br>
                    </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="caption">
                    <h2>Saison</h2>
                    <hr>
                    <p>
<?php
/** @todo mettre en config **/
date_default_timezone_set('Europe/Paris');
setlocale(LC_TIME, 'fr_FR.UTF-8', 'fra');
?>
                        <b>Saison : </b> <?=$saison->nomsaison;?> <br>
                        du <b><?=(utf8_encode(strftime("%A %d %B %Y", strtotime($saison->datedebsaison))));?></b>
                        au <b><?=(utf8_encode(strftime("%A %d %B %Y", strtotime($saison->datefinsaison))));?></b><br>
                        <b>Prix de la semaine</b> : <?=$prix->prixheb;?>€
                        <hr />
                        Les arrhes sont à envoyés dans un délais de 21 jours après la réservation.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <h2 class="text-center">Réservations</h2>
    <div class="row">
        <div class="col-lg-offset-2 col-lg-8">

        <?php $attributes = ["id" => "reservation_form", "name" => "reservation_form", "class" => "form-inline"];?>
        <?=form_open("villageois/reserver_heberg/$heberg->noheb/$saison->codesaison", $attributes);?>
            <div class="form-group">
                <label for="txt_semaine_deb">Semaine de début</label>
                 <div class='input-group' >
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    <input type='text' class="form-control" name="txt_semaine_deb" id='txt_semaine_deb' placeholder="dd-mm-yyyy" value="<?=set_value('txt_semaine_deb');?>"/>
                </div>
                <span class="text-danger"><?=form_error('txt_semaine_deb');?></span>
            </div>

            <div class="form-group">
                <label for="txt_nb_pers">Nombre de personne:</label>
                <select class="form-control" id="txt_nbpers" name="txt_nbpers">
                  <?php for ($i = 1; $i < $heberg->nbplaceheb + 1; $i++): ?>
                    <option value="<?=$i;?>" <?=set_select('txt_nbpers', $i);?> ><?=$i;?></option>
                  <?php endfor;?>
                  <span class="text-danger"><?=form_error('txt_nbpers');?></span>
                </select>
            </div>
            <input type="hidden" id="cdSaison" name="cdSaison" value="<?=$saison->codesaison;?>">
            <input type="hidden" id="noHeb"  name="noHeb" value="<?=$heberg->noheb;?>">
            <button type="submit" class="btn btn-default">Réserver</button>
        <?=form_close();?>
        <br />
        <br />
        Votre délai moyen de réservation est de <?=round($delais_moyen);?> jours.
        </div>
    </div>
</div>