<div class="row">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				 <h1 class="page-header">
					Connexion
				</h1>
				<ol class="breadcrumb">
					<li><a href="<?=base_url();?>">Accueil</a></li>
					<li>Connexion</li>
				</ol>
			</div>
		</div>
	<div class="row">
		<div class="col-lg-offset-3 col-lg-6 col-sm-6">
			<?php $attributes = ["id" => "loginform", "name" => "loginform"];?>
			<?=form_open("connexion/index", $attributes);?>
			   <div class="form-group">
					<div class="row">
					    <div class="col-lg-offset-2 col-lg-8 col-sm-offset-2 col-sm-8">
						    <label for="txt_login" class="control-label">Login</label>
						    <div class="input-group">
							    <span class="input-group-addon">
									<i class="glyphicon glyphicon-user"></i>
							    </span>
							    <input class="form-control" id="txt_login" name="txt_login" placeholder="Login" type="text" value="<?=set_value('txt_login');?>" />
						    </div>
						    <span class="text-danger"><?php echo form_error('txt_login'); ?></span>
					    </div>
					</div>
			    </div>

			    <div class="form-group">
					<div class="row">
						<div class="col-lg-offset-2 col-lg-8 col-sm-offset-2 col-sm-8">
							<label for="txt_password" class="control-label">Mot de passe</label>
							<div class="input-group">
								<span class="input-group-addon">
									<i class="glyphicon glyphicon-lock"></i>
								</span>
								<input class="form-control" id="txt_password" name="txt_password" placeholder="Mot de passe" type="password" value="" />
							</div>
							 <span class="text-danger"><?php echo form_error('txt_password'); ?></span>
						</div>
					</div>
			   </div>
			   <div class="form-group">
					<div class="col-lg-12 col-sm-12 text-center">
						 <input id="btn_login" name="btn_login" type="submit" class="btn btn-success" value="Connexion" />
					</div>
			   </div>
			<?php echo form_close(); ?>
		    <br />
		    <br />
		    <?php echo $this->session->flashdata('msg'); ?>
		</div>
	</div>
</div>






