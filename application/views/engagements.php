<div class="container">
    <div class="row">
        <div class="col-lg-12">
             <h1 class="page-header">
                Les engagements de Village Vacances Alpes
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?=base_url();?>">Accueil</a></li>
                <li>Nos engagements</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
           <ol>
            <li>Engagements pour L'HISTOIRE</li>


            - Installer des villages exclusivement en France, dans des territoires naturels préservés, ou à proximité de grands sites touristiques en basse, moyenne ou haute montage.
            <br>
            - Préférer toujours des villages à taille humaine et un excellent rapport qualité-prix pour des vacances de qualité accessibles au plus grand nombre.
            <br>
            - Respecter toutes les diversités, toutes les différences, au niveau des salariés comme au niveau des villageois et travailler main dans la main avec les communes d’accueil.
            <br><br>
            <li>Engagements pour LE SOURIRE  </li>
            - Constituer dans chaque village une équipe compétente et accueillante, qui permette de passer des vacances en toute sérénité et garantir un accueil chaleureux et personnalisé.
            <br>
            - Offrir au quotidien et à tous les niveaux une excellente qualité de service.
            <br><br>
            <li>Engagements pour LE PLAISIR</li>
            - Proposer une diversité d’animations, avec des animateurs pour qui plaisir rime avec rire.
            <br>
            - Offrir un grand choix d’activités sportives ou culturelles, encadrées par des passionnés de leur discipline et permettre de vivre sa passion lors de séjours thématiques.
            <br>
            -  Faire découvrir la nature par des randonnées hors des sentiers battus.
            <br><br>
            <li>Engagements pour LES ENFANTS</li>
            - Proposer des équipements spécialement adaptés aux bébés de 3 à 36 mois.
            <br>
            - Inscrire les enfants dans un projet pédagogique épanouissant, ouvert sur les autres et sur le monde et les sensibiliser au développement durable et à la préservation de la planète.
            <br><br>
            <li>Engagements pour LE CONFORT</li>
            - Rénover sans cesse nos villages, dans le cadre d’un plan de modernisation considérable.
            <br>
            - Assurer la sécurité au quotidien par des audits d’hygiène en restauration, des analyses systématiques de tous les points d’eau, des contrôles réguliers des aires de jeux et des espaces sportifs. Prendre en compte les handicaps, dans des villages labellisés Tourisme et Handicap.
            <br><br>
            <li>Engagements pour LE GOÛT</li>
            - Permettre, d’être vraiment en vacances avec la pension complète et proposer de nouveaux menus, originaux et créatifs.
            <br>
            - Garantir une cuisine familiale et une bonne qualité des ingrédients avec un service personnalisé, attentionné et convivial et faire goûter les spécialités régionales.
            <br><br>
            <li>Engagements pour LE POUVOIR D'ACHAT </li>
            - Pratiquer des tarifs modérés, pour offrir des vacances adaptées à tous les budgets, même les plus modestes.
            <br>
            - Accepter toutes les aides au départ : CAF, Comités d’Entreprise,  Comités d’œuvres sociales, Chèques-Vacances et soutenir les associations caritatives et les familles en difficulté.
            <br>
            - Pratiquer des tarifs réduits négociés avec nos partenaires pour des activités extérieures.
            <br><br>
            <li>Engagements pour LE DÉVELOPPEMENT LOCAL</li>
            - Assumer, une mission fondamentale d’acteur de l’économie sociale et solidaire et jouer, un rôle moteur dans l’aménagement et le développement du territoire de proximité.
            <br>
            - Échanger régulièrement avec les voisins, les riverains, les habitants de la commune.
            <br>
            - Privilégier le recours à l‘emploi local, en recrutant des saisonniers originaires de la région et les circuits courts de distribution, en achetant aux producteurs locaux.
            <br><br>
            <li> Engagements pour L'EMPLOI</li>
            - Agir en employeur responsable, pour développer la motivation, l’implication et les compétences des équipes et garantir aux 1300 salariés de VVA de bonnes conditions de travail.
            <br>
            - Rejeter toute forme de discrimination à l’embauche, en favorisant en particulier l’accueil des salariés handicapés et être attentif à la parité et à l’égalité de salaire entre hommes et femmes.
            <br><br>
            <li>Engagements pour LA FORMATION</li>
            - Professionnaliser les métiers de VVA : l’accueil, l’hébergement, l’animation, la restauration, la maintenance et les espaces verts.
            <br>
            - Développer les contrats en alternance et les contrats d’apprentissage et veiller à l’accueil de stagiaires pour leur transmettre nos savoirs.
            <br><br>
            <li>Engagements pour LE DÉVELOPPEMENT SOLIDAIRE
            - Être une entreprise responsable dans tous les domaines et choisir des fournisseurs en fonction de leur politique salariale et de leur engagement durable.
            <br>
            - Respecter au maximum l’environnement de chaque village et ne pas construire n’importe où, n’importe comment.
            <br>
            - Être au plus près des enjeux écologiques en étudiant toutes les alternatives en termes de matériaux de construction ou d’appareils électroménagers.
            <br>
            - Recenser et diffuser les bonnes pratiques des villages et sensibiliser à ces pratiques tous nos partenaires en informant également nos villageois.
            <br><br>
            <li>Engagements pour LA PLANÈTE </li>
            - Sensibiliser parents et enfants aux déplacements à pied et à vélo, dans et autour des villages.
            <br>
            - Organiser sur des sites sensibles des animations “faune et flore” avec des organismes de défense de l’environnement (par exemple la LPO, Ligue de protection des Oiseaux).
            <br>
            - Récupérer l’eau de pluie, installer des composteurs sur tous les sites, installer des économiseurs d’eau, mettre en place des ampoules basse consommation, équiper les villages de voiturettes électriques, utiliser des matériaux de récupération pour les activités manuelles et donner l’exemple au siège de VVA, en économisant l’eau, en récupérant le papier brouillon, etc.
            </ol>
      </div>
   </div>
</div>