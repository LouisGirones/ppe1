<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('alert')) {
    /**
     * Création d'une alerte bootstrap
     *
     * @todo Rajouter un paramètre booléen indiquant si l'alerte comporte une croix pour la fermer
     * @todo Mettre le paramètre facultatif en dernier
     *
     * @param string $alert Le type d'alerte (success|info|warning|danger)
     * @param string $titre Le titre de l'alerte
     * @param string $msg   Le message de l'alerte
     * @return html
     */
    function alert($alert, $titre = '', $msg)
    {
        return '<div class="alert alert-' . $alert . ' alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                </button>
                    <strong>' . $titre . ' </strong><br /> ' . $msg . '
                </div>';
    }
}
