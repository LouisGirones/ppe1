<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('menu_url')) {
    /**
     * On récupère le menu correspondant au type de compte de l'utilisateur
     * 1 = admin, 2 = gestionnaire, 3 = villageois, 4 = visiteur
     *
     * @param type $typeCompte
     * @return type
     */
    function menu_url($typeCompte)
    {
        switch ($typeCompte) {
            case 'AD':
                return 'menu/menu_admin';
                break;
            case 'GES':
                return 'menu/menu_gestionnaire';
                break;
            case 'VIL':
                return 'menu/menu_villageois';
                break;
            default:
                return 'menu/menu_usager';
                break;
        }
    }
}

if (!function_exists('active_link')) {
    /**
     * Ajoute un affichage css au lien du menu actif
     *
     * @param string $controller
     * @param string $methode
     * @return string
     */
    function active_link($controller, $methode = "")
    {
        $CI = &get_instance();
        $meth = $CI->router->fetch_method();
        $class = $CI->router->fetch_class();
        if (!empty($methode)) {
            return ($class == $controller && $meth == $methode) ? 'active' : '';
        } else {
            return ($class == $controller) ? 'active' : '';
        }
    }
}
