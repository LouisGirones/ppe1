<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('css_url')) {
    /**
     * Récupération d'un fichier css
     *
     * @param string $nom Le nom du fichier
     * @return url
     */
    function css_url($nom)
    {
        return base_url() . 'assets/css/' . $nom . '.css';
    }
}

if (!function_exists('js_url')) {
    /**
     * Récupération d'un fichier javascript
     *
     * @param string $nom Le nom du fichier
     * @return url
     */
    function js_url($nom)
    {
        return base_url() . 'assets/js/' . $nom . '.js';
    }
}

if (!function_exists('img_url')) {
    /**
     * Récupération d'une image
     *
     * @param string $nom Le nom du fichier
     * @return url
     */
    function img_url($nom)
    {
        return base_url() . 'assets/img/' . $nom;
    }
}

if (!function_exists('img')) {
    /**
     * Création de la balise pour afficher une image
     *
     * @todo Utiliser un tableau pour les classes
     *
     * @param string $nom   Le nom du de l'image
     * @param string $alt   Le texte alternatif à l'image
     * @param string $class Les classes css à ajouter
     * @return html
     */
    function img($nom, $alt = '', $class = '')
    {
        return '<img src="' . img_url($nom) . '" alt="' . $alt . '" class= "' . $class . '" />';
    }
}

if (!function_exists('add_js')) {
    /**
     * Ajouter des fichiers js
     *
     *
     * @param string|array $file
     * @return void
     */
    function add_js($file = '')
    {
        $str = '';
        $ci = &get_instance();
        $header_js = $ci->config->item('header_js');

        if (empty($file)) {
            return;
        }

        if (is_array($file)) {
            if (!is_array($file) && count($file) <= 0) {
                return;
            }
            foreach ($file as $item) {
                $header_js[] = $item;
            }
            $ci->config->set_item('header_js', $header_js);
        } else {
            $str = $file;
            $header_js[] = $str;
            $ci->config->set_item('header_js', $header_js);
        }
    }
}

if (!function_exists('add_css')) {
    /**
     * Ajouter des fichiers css
     *
     * @param string|arrays $file
     * @return void
     */
    function add_css($file = '')
    {
        $str = '';
        $ci = &get_instance();
        $header_css = $ci->config->item('header_css');

        if (empty($file)) {
            return;
        }

        if (is_array($file)) {
            if (!is_array($file) && count($file) <= 0) {
                return;
            }
            foreach ($file as $item) {
                $header_css[] = $item;
            }
            $ci->config->set_item('header_css', $header_css);
        } else {
            $str = $file;
            $header_css[] = $str;
            $ci->config->set_item('header_css', $header_css);
        }
    }
}

if (!function_exists('put_headers_css')) {
    /**
     * Ajout des fichiers css
     *
     * @return html
     */
    function put_headers_css()
    {
        $str = '';
        $ci = &get_instance();
        $header_css = $ci->config->item('header_css');

        foreach ($header_css as $item) {
            $str .= '<link rel="stylesheet" href="' . css_url($item) . '" type="text/css" />' . "\n";
        }

        return $str;
    }
}

if (!function_exists('put_headers_js')) {
    /**
     * Ajout des fichier js
     *
     * @return html
     */
    function put_headers_js()
    {
        $str = '';
        $ci = &get_instance();
        $header_js = $ci->config->item('header_js');

        foreach ($header_js as $item) {
            $str .= '<script type="text/javascript" src="' . js_url($item) . '"></script>' . "\n";
        }

        return $str;
    }
}
