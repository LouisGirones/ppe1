<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Saison_model extends CI_Model
{
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /**
     * Récupère les informations sur une saison
     *
     * @param int $codeSaison L'identifiant de la saison
     * @return object
     */
    public function get_infos_saison($codeSaison)
    {
        return $this->db->select('codesaison, nomsaison, datedebsaison, datefinsaison')
            ->from('saison')
            ->where('codesaison', $codeSaison)
            ->get()
            ->row();
    }

    /**
     * Récupère les prix des saisons à venir pour un hébergement
     *
     * @param int $noHeb L'identifiant d'hébergement
     * @return array
     */
    public function ListePrixSaison($noHeb)
    {
        return $this->db->select('saison.codesaison, prixheb, nomsaison')
            ->from('saison')
        //clause AND dans la jointure
            ->join('tarif', 'saison.codesaison=tarif.codesaison AND tarif.noheb=' . (int) $noHeb, 'left')
            ->where('datefinsaison >', 'NOW()', false)
            ->get()
            ->result();
    }

    /**
     * Récupère la liste des saisons
     *
     * @return array
     */
    public function get_liste_saisons()
    {
        return $this->db->select('nomsaison, datedebsaison, datefinsaison')
            ->from('saison')
            ->get()
            ->result();
    }

    /**
     * Vérifier si une saison est déjà en cours pour une période donnée
     *
     * @param string $dtDeb La date de début
     * @param string $dtFin La date de fin
     * @return int
     */
    public function saison_existe($dtDeb, $dtFin)
    {
        return $this->db->select('count(codesaison) as nb_saison')
            ->from('semaine')
            ->where('datedebsem >=', date('Y-m-d', strtotime($dtDeb)))
            ->where('datedebsem <=', date('Y-m-d', strtotime($dtFin)))
            ->get()
            ->row()->nb_saison;
    }

    /**
     * Ajouter une saison
     *
     * @param string $nom   Le nom de la saison
     * @param string $dtDeb La date de début
     * @param string $dtFin La date de fin
     * @return int
     */
    public function ajouter_saison($nom, $dtDeb, $dtFin)
    {
        //Ces données seront automatiquement échappées
        $this->db->set('nomsaison', $nom)
            ->set('datedebsaison', date('Y-m-d', strtotime($dtDeb)))
            ->set('datefinsaison', date('Y-m-d', strtotime($dtFin)))
            ->insert('saison');
        return $this->db->insert_id();
    }

    /**
     * Ajoute les semaines d'une saison
     *
     * @param int    $codesaison L'identifiant de la saison
     * @param string $dtDeb      La date de début
     * @param string $dtFin      La date de fin
     * @return int
     */
    public function ajouter_semaines_saison($codeSaison, $dtDeb, $dtFin)
    {
        $nbSem = 0;
        $dtDeb = date('Y-m-d', strtotime($dtDeb));
        $dtFin = date('Y-m-d', strtotime($dtFin));
        while (strtotime($dtDeb) < strtotime($dtFin)) {
            $semainePro = date('Y-m-d', strtotime($dtDeb . '+1 week')); // semaine de debut +1 semaine
            // Insertion des semaines dans la bdd
            $this->ajouter_semaine($codeSaison, $dtDeb, $semainePro);
            $dtDeb = $semainePro; // la semaine de debut change
            $nbSem++;
        }
        return $nbSem;
    }

    /**
     * Ajouter une semaine
     *
     * @param int    $codeSaison L'identifiant de la saison
     * @param string $dtDeb      La date de début
     * @param string $dtFin      La date de fin
     * @return int
     */
    public function ajouter_semaine($codeSaison, $dtDeb, $dtFin)
    {
        $this->db->set('codesaison', $codeSaison)
            ->set('datedebsem', $dtDeb)
            ->set('datefinsem', $dtFin)
            ->insert('semaine');
        return $this->db->insert_id();
    }
}
