<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ajax_model extends CI_Model
{

    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /**
     * Récupère les noms d'hébergements
     *
     * @param string $nom La chaîne de caractère
     * @return array
     */
    public function get_noms_hebergs($nom)
    {
        return $this->db->select('nomheb')
            ->from('hebergement')
            ->like('LOWER(nomheb)', strtolower($nom))
            ->get()
            ->result();
    }

    /**
     * Récupère les semaines réservables pour une saison et un hébergement
     * Une semaine réservable est une semaine qui n'a pas encore été réservée
     *
     * @param int $noHeb    L'identifiant d'hébergement
     * @param int $cdSaison Le code de la saison
     * @return array
     */
    public function get_semaines_dispo($noHeb, $cdSaison)
    {
        $this->db->select('datedebsem')
            ->from('resa')
            ->where('resa.datedebsem', 'semaine.datedebsem', false)
            ->where('noheb', $noHeb);
        $subQuery = $this->db->get_compiled_select();

        return $this->db->select('DATE_FORMAT(datedebsem,\'%e-%c-%Y\') AS datedebsem')
            ->from('semaine')
            ->where('codesaison', $cdSaison)
            ->where('datedebsem > NOW()', null, false)
            ->where('NOT EXISTS ( ' . $subQuery . ')', null, false)
            ->get()
            ->result();
    }
}
