<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tarif_model extends CI_Model
{
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /**
     * Récupère le tarif d'un hébergement pour une saison
     *
     * @param int   $noHeb    L'identifiant d'hébergement
     * @param int   $cdSaison L'identifiant de la saison
     * @return float
     */
    public function get_tarif($noHeb, $cdSaison)
    {
        return $this->db->select('prixheb')
            ->from('tarif')
            ->where('noheb', $noHeb)
            ->where('codesaison', $cdSaison)
            ->get()
            ->row();
    }
}
