<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Reservation_model extends CI_Model
{
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /**
     * Récupère le nombre de réservations
     *
     * @param int    $idVill     L'identifiant du villageois
     * @param bool   $historique Si on récupère les réservations actuelles ou passées
     * @param int    $etatResa   L'état de la réservation
     * @param string $semaineDeb La semaine de début de la réservation
     * @return int
     */
    public function get_nb_resas($idVill = '', $historique = false, $etatResa = '', $semaineDeb = '')
    {
        $historique = $historique == false ? '>' : '<';

        $query = $this->db->select('COUNT(noheb) as nb_resa')
            ->from('resa')
            ->where('datedebsem ' . $historique . ' now() - INTERVAL 7 DAY', null, false);
        if (is_int($idVill)) {
            $this->db->where('novillageois', $idVill);
        }
        if (!empty($etatResa)) {
            $this->db->where('codeetatresa', $etatResa);
        }
        if (!empty($semaineDeb)) {
            $this->db->where('datedebsem', date('Y-m-d', strtotime($semaineDeb)));
        }
        return $query->get()->row()->nb_resa;
    }

    /**
     * Récupère la liste des réservations
     *
     * @param int    $idVill     L'identifiant du villageois
     * @param int    $limit      Le nombre de réservations à récupérer
     * @param int    $start      La réservation à partir de laquelle on commence
     * @param bool   $historique Si on récupère les réservations actuelles ou passées
     * @param int    $etatResa   L'état de la réservation
     * @param string $semaineDeb La semaine de début de la réservation
     * @return array
     */
    public function get_listes_resas($idVill = '', $limit, $start, $historique = false, $etatResa = '', $semaineDeb = '')
    {
        $historique = $historique == false ? '>' : '<';

        $this->db->select('resa.noheb, novillageois, DATE_FORMAT(datedebsem, \'%d-%m-%Y\') as datedebsem, DATE_FORMAT(dateresa, \'%d-%m-%Y\') AS dateresa, DATE_FORMAT(dateaccuserecept, \'%d-%m-%Y\') as dateaccuserecept, DATE_FORMAT(datearrhes, \'%d-%m-%Y\') as datearrhes, montantarrhes, resa.codeetatresa, nboccupant, nbplaceheb, prixresa, nometatresa, (21-datediff(curdate(), dateresa)) as jours_restant_arrhes')
            ->from('resa')
            ->join('etat_resa', 'etat_resa.codeetatresa = resa.codeetatresa')
            ->join('hebergement', 'hebergement.noheb = resa.noheb')
            ->where('datedebsem ' . $historique . ' now() - INTERVAL 7 DAY', null, false);
        if (is_int($idVill)) {
            $this->db->where('novillageois', $idVill);
        }
        if (!empty($etatResa)) {
            $this->db->where('resa.codeetatresa', $etatResa);
        }
        if (!empty($semaineDeb)) {
            $this->db->where('resa.datedebsem', date('Y-m-d', strtotime($semaineDeb)));
        }
        $this->db->order_by('dateresa')
            ->limit($limit, $start);
        return $this->db->get()->result();
    }

    /**
     * Récupère les informations sur une réservation
     *
     * @param int    $idVill     L'identifiant du villageois
     * @param int    $noHeb      Le numéro d'hébergement
     * @param string $semaineDeb La semaine de début de la réservation
     * @return object
     */
    public function get_details_resa($idVill, $noHeb, $semaineDeb)
    {
        return $this->db->select('resa.noheb, nomheb, nomtypeheb, anneeheb, secteurheb, orientationheb, etatheb, internet, nometatresa, DATE_FORMAT(datedebsem, \'%d-%m-%Y\') as datedebsem, datedebsem as datedebsemlien, DATE_FORMAT(dateresa, \'%d-%m-%Y\') AS dateresa, DATE_FORMAT(dateaccuserecept, \'%d-%m-%Y\') as dateaccuserecept, DATE_FORMAT(datearrhes, \'%d-%m-%Y\') as datearrhes, montantarrhes, resa.codeetatresa, nboccupant, nbplaceheb, prixresa, (21-datediff(curdate(), dateresa)) as jours_restant_arrhes')
            ->from('resa')
            ->join('etat_resa', 'etat_resa.codeetatresa = resa.codeetatresa')
            ->join('hebergement', 'hebergement.noheb = resa.noheb')
            ->join('type_heb', 'type_heb.codetypeheb = hebergement.codetypeheb')
            ->where('novillageois', $idVill)
            ->where('resa.noheb', $noHeb)
            ->where('datedebsem', date('Y-m-d', strtotime($semaineDeb)))
            ->get()
            ->row();
    }

    /**
     * Supprimer une réservation (un villageois ne peut supprimer que ses propres réservations)
     *
     * @param int    $idVill     L'identifiant du villageois
     * @param int    $noheb      L'identifiant d'hébergement
     * @param string $semaineDeb La date de début de la réservation
     * @return int
     */
    public function supprimer_resa($idVill, $noHeb, $semaineDeb)
    {
        $this->db->where('novillageois', $idVill)
            ->where('noheb', $noHeb)
            ->where('datedebsem', date('Y-m-d', strtotime($semaineDeb)))
            ->delete('resa');
        return $this->db->affected_rows();
    }

    /**
     * Supprimer une réservation (un gestionnaire peut supprimer les réservations dépassés)
     *
     * @param int    $noheb      L'identifiant d'hébergement
     * @param string $semaineDeb La date de début de la réservation
     * @return int
     */
    public function supprimer_resa_gest($noHeb, $semaineDeb)
    {
        $this->db->where('noheb', $noHeb)
            ->where('datedebsem', date('Y-m-d', strtotime($semaineDeb)))
            ->delete('resa');
        return $this->db->affected_rows();
    }

    /**
     * Vérifie qu'une semaine est disponible pour réservation
     *
     * @param string $dtSem    La date de début
     * @param int    $cdSaison Le code de la saison
     * @return int
     */
    public function verifier_semaine($dtSem, $cdSaison)
    {
        return $this->db->select('count(datedebsem) as nb_semaine')
            ->from('semaine')
            ->where('datedebsem', date('Y-m-d', strtotime($dtSem)))
            ->where('codesaison', $cdSaison)
            ->get()
            ->row()->nb_semaine;
    }

    /**
     * Vérifie qu'un hébergement est libre pour une semaine donnée
     *
     * @param type $dtSem La semaine de début
     * @param type $noHeb L'identifiant d'hébergement
     * @return type
     */
    public function verifier_heberg_libre($dtSem, $noHeb)
    {
        return $this->db->select('count(noheb) as nb_resa')
            ->from('resa')
            ->where('datedebsem', date('Y-m-d', strtotime($dtSem)))
            ->where('noheb', $noHeb)
            ->get()
            ->row()->nb_resa;
    }

    /**
     * Enregistrement d'une réservation
     *
     * @param type $noHeb           L'identifiant d'hébergement
     * @param type $dtDebutSemaine  La semaine de début
     * @param type $noVill          L'identifiant du villageois
     * @param type $nbPers          Le nombre de personne
     * @param type $prix            Le prix
     * @return void
     */
    public function ajouter_resa($noHeb, $dtDebutSemaine, $noVill, $nbPers, $prix)
    {
        $this->db->set('noheb', $noHeb)
            ->set('datedebsem', date('Y-m-d', strtotime($dtDebutSemaine)))
            ->set('novillageois', $noVill)
            ->set('codeetatresa', 1)
            ->set('dateresa', 'NOW()', false)
            ->set('nboccupant', $nbPers)
            ->set('prixresa', $prix)
            ->insert('resa');
    }

    /**
     * Récupère les différents états de réservation possibles
     *
     * @return array
     */
    public function get_etats_resa()
    {
        return $this->db->select('codeetatresa, nometatresa')
            ->from('etat_resa')
            ->get()
            ->result();
    }

    /**
     * Mettre a jour une réservation avec la date d'accusé de réception
     *
     * @param string  $date       Date d'arrivée de l'accusé de réception
     * @param int     $nvEtatResa Le nouvel état de la réservation
     * @param int     $noheb      L'identifiant de l'hébergment
     * @param string  $dtDebSem   La date de début de réservation
     * @return void
     */
    public function set_date_acc_recept($date, $nvEtatResa, $noHeb, $dtDebSem)
    {
        $this->db->set('dateaccuserecept', date('Y-m-d', strtotime($date)))
            ->set('codeetatresa', $nvEtatResa)
            ->where('noheb', $noHeb)
            ->where('datedebsem', date('Y-m-d', strtotime($dtDebSem)))
            ->update('resa');
    }

    /**
     * Met à jour une réservation avec la date de réception des arrhes et leurs montants
     *
     * @param string $dtArrhes    Date d'arrivée des arrhes
     * @param float  $mtArrhes    Le montant des arrhes reçus
     * @param int    $nvEtatResa  Le nouvel état de la réservation
     * @param int    $noHeb       L'identifiant d'hébergement
     * @param string $semDeb      La date de début de réservation
     * @return void
     */
    public function set_date_mt_arrhes($dtArrhes, $mtArrhes, $nvEtatResa, $noHeb, $semDeb)
    {
        $this->db->set('datearrhes', date('Y-m-d', strtotime($dtArrhes)))
            ->set('montantarrhes', $mtArrhes)
            ->set('codeetatresa', $nvEtatResa)
            ->where('noheb', $noHeb)
            ->where('datedebsem', date('Y-m-d', strtotime($semDeb)))
            ->update('resa');
    }

    /**
     * Met à jour l'état d'une réservation en spécifiant la date ou le client est arrivé
     *
     * @param int    $nvEtatResa Le nouvel état de réservation
     * @param int    $noHeb      L'identifiant d'hébergement
     * @param string $semDeb     La semaine de début
     * @return void
     */
    public function set_resa_en_cours($nvEtatResa, $noHeb, $semDeb)
    {
        $this->db->set('codeetatresa', $nv_etatResa)
            ->where('noheb', $no_heb)
            ->where('datedebsem', date('Y-m-d', strtotime($sem_deb)))
            ->update('resa');
    }
}
