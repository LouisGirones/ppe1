<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Villageois_model extends CI_Model
{
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /**
     * Récupère les informations sur un villageois
     *
     * @param string $login Le login du villageois
     * @return object
     */
    public function get_infos_villageois($login)
    {
        return $this->db->select('novillageois, nomvillageois, prenomvillageois, adrmail, notel, noport')
            ->from('villageois')
            ->where('user', $login)
            ->get()
            ->row();
    }

    /**
     * Ajout d'un villageois
     *
     * @param string $user     Le login associé au compre
     * @param string $mail     L'adesse mail du villageois
     * @param int    $tel      Le téléphone du villageois
     * @param int    $portable Le téléphone portable du villageois
     * @return int
     */
    public function ajout_villageois($user, $mail, $tel, $portable)
    {
        $select = $this->compte_model->get_infos_compte($user);

        $this->db->set('user', $user)
            ->set('adrmail', $mail)
            ->set('notel', $tel)
            ->set('noport', $portable)
            ->set('nomvillageois', $select->nomcompte)
            ->set('prenomvillageois', $select->prenomcompte)
            ->insert('villageois');

        return $this->db->insert_id();
    }
}
