<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Hebergement_model extends CI_Model
{
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /**
     * Vérifie que le nom d'hébergement n'existe pas déjà
     *
     * @param string $nom   Le nom de l'hébergement
     * @param int    $noHeb L'identifiant de l'hébergement
     * @return int
     */
    public function verifier_nom_heberg_libre($nom, $noHeb)
    {
        $this->db->select('count(noheb) as nb_heb')
            ->from('hebergement')
            ->where('nomheb', $nom)
            ->where('noheb !=', $noheb)
            ->get();
        return $this->db->row()->nb_heb;
    }

    /**
     * Récupère le nombre d'hébergement
     *
     * @return int
     */
    public function get_nb_heberg()
    {
        $this->db->select('COUNT(noheb) as nb_heb')
            ->from('hebergement');
        return $this->db->get()->row()->nb_heb;
    }

    /**
     * Récupère les types d'hébergement possible
     *
     * @return array
     */
    public function get_types_heberg()
    {
        return $this->db->select('codetypeheb, nomtypeheb')
            ->from('type_heb')
            ->get()
            ->result();
    }

    /**
     * Récupère le nom d'un type d'hébergement grâce à son identifiant
     *
     * @param int $id
     * @return type
     */
    public function get_nom_type_heberg($id)
    {
        return $this->db->select('nomtypeheb')
            ->from('type_heb')
            ->where('codetypeheb', $id)
            ->get()
            ->row()->nomtypeheb;
    }

    /**
     * Récupère la liste des hébergements
     *
     * @param int $limit Le nombre d'hébergement à récupérer
     * @param int $start A partir de quel hébergement commence-t-on
     * @return array
     */
    public function get_hebergs($limit, $start)
    {
        return $this->db->select('noheb, nomheb, descriheb, photoheb, nomtypeheb')
            ->from('hebergement')
            ->join('type_heb', 'type_heb.codetypeheb=hebergement.codetypeheb')
            ->order_by('noheb', 'DESC')
            ->limit($limit, $start)
            ->get()
            ->result();
    }

    /**
     * Récupère les informations sur un hébergement via son identifiant
     *
     * @param int $noHeb L'identifiant de l'hébergement
     * @return object
     */
    public function get_details_heberg($noHeb)
    {
        return $this->db->select('noheb, nomheb, nbplaceheb, surfaceheb, internet, anneeheb, secteurheb, orientationheb, etatheb, descriheb, photoheb, nomtypeheb, hebergement.codetypeheb')
            ->from('hebergement')
            ->join('type_heb', 'type_heb.codetypeheb=hebergement.codetypeheb')
            ->where('noheb', $noHeb)
            ->get()
            ->row();
    }

    /**
     * Récupère un identifiant d'hébergement via son nom
     *
     * @param string $nom
     * @return int
     */
    public function get_id_heberg_par_nom($nom)
    {
        $query = $this->db->select('noheb')
            ->from('hebergement')
            ->where('nomheb', $nom)
            ->get();
        return $query->row() === null ? 0 : $query->row()->noheb;
    }

    /**
     * Fixer le prix d'une saison pour un hébergement
     *
     * @param int   $noHeb    L'identifiant d'hébergement
     * @param int   $cdSaison Le code de la saison
     * @param float $prix     Le prix
     * @return type
     */
    public function set_prix_saison($noHeb, $cdSaison, $prix)
    {
        $this->db->set('noheb', $noHeb)
            ->set('codesaison', $cdSaison)
            ->set('prixheb', $prix)
            ->insert('tarif');
        return $this->db->insert_id();
    }

    /**
     * Ajouter un hébergement
     *
     * @param string $nom         Le nom de l'hébergement (unique)
     * @param int    $codeType    Le code du type de l'hébergement
     * @param int    $nbPlace     Le nombre de place de l'hébergement
     * @param int    $surface     La surface de l'hébergement
     * @param bool   $internet    Si l'hébergement possède internet
     * @param int    $anneeHeb    La dernière année où l'hébergement a été réparé
     * @param string $secteur     Le secteur de l'hébergement
     * @param string $orientation L'orientation de l'hébergement
     * @param string $etat        L'état de l'hébergement
     * @param string $description La description de l'hébergement
     * @param string $image       L'image de l'hébergement (facultatif)
     * @return int
     */
    public function ajouter_hebergement($nom, $codeType, $nbPlace, $surface, $internet, $anneeHeb, $secteur, $orientation, $etat, $description, $image = null)
    {
        $this->db->set('nomheb', $nom)
            ->set('codetypeheb', $codeType)
            ->set('nbplaceheb', $nbPlace)
            ->set('surfaceheb', $surface)
            ->set('internet', $internet)
            ->set('anneeheb', $anneeHeb)
            ->set('secteurheb', $secteur)
            ->set('orientationheb', $orientation)
            ->set('etatheb', $etat)
            ->set('descriheb', $description)
            ->set('photoheb', $image)
            ->insert('hebergement');
        return $this->db->insert_id();
    }

    /**
     * Modifier un hébergement
     *
     * @param int $noHeb          L'identifiant de l'hébergement
     * @param string $nom         Le nom de l'hébergement (unique)
     * @param int    $codeType    Le code du type de l'hébergement
     * @param int    $nbPlace     Le nombre de place de l'hébergement
     * @param int    $surface     La surface de l'hébergement
     * @param bool   $internet    Si l'hébergement possède internet
     * @param int    $anneeHeb    La dernière année où l'hébergement a été réparé
     * @param string $secteur     Le secteur de l'hébergement
     * @param string $orientation L'orientation de l'hébergement
     * @param string $etat        L'état de l'hébergement
     * @param string $description La description de l'hébergement
     * @param string $image       L'image de l'hébergement (facultatif)
     * @return void
     */
    public function modifier_hebergement($noHeb, $nom, $codeType, $nbPlace, $surface, $internet, $anneeHeb, $secteur, $orientation, $etat, $description, $image = null)
    {
        $this->db->set('nomheb', $nom)
            ->set('codetypeheb', $codeType)
            ->set('nbplaceheb', $nbPlace)
            ->set('surfaceheb', $surface)
            ->set('internet', $internet)
            ->set('anneeheb', $anneeHeb)
            ->set('secteurheb', $secteur)
            ->set('orientationheb', $orientation)
            ->set('etatheb', $etat)
            ->set('descriheb', $description);
        if ($image !== null) {
            $this->db->set('photoheb', $image);
        }
        $this->db->where('noheb', $noHeb);
        $this->db->update('hebergement');
    }
}
