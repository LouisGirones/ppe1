 <?php
defined('BASEPATH') or exit('No direct script access allowed');

class Compte_model extends CI_Model
{
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /**
     * Connexion d'un utilisateur
     *
     * @param string $login
     * @param string $pwd
     * @return bool
     */
    public function connect($login, $pwd)
    {
        //On recup le hash du mdp
        $hash = $this->db->select('mdp')
            ->from('compte')
            ->where('user', $login)
            ->get()
            ->row();
        //Le hash est NULL signifie aucun compte pour ce login
        if ($hash === null) {
            return false;
        }
        //On vérifie que le pwd correspond au hash
        return password_verify($pwd, $hash->mdp);
    }

    /**
     * Récupère les informations d'un utilsateur
     *
     * @param string $login
     * @return objet
     */
    public function get_infos_compte($login)
    {
        return $this->db->select('nomcompte, prenomcompte, dateinscrip, typecompte ')
            ->from('compte')
            ->where('user', $login)
            ->get()
            ->row();
    }

    /**
     * Récupère l'ensemble des comptes (admin)
     *
     * @return array
     */
    public function get_liste_comptes()
    {
        return $this->db->select('compte.user, nomcompte, prenomcompte, dateinscrip, typecompte, adrmail, notel, noport ')
            ->from('compte')
            ->join('villageois', 'villageois.user = compte.user', 'left')
            ->order_by('typecompte')
            ->get()
            ->result();
    }

    /**
     *
     * Ajout demandé lors de l'épreuve E4 du BTS SIO
     *
     * Récupère les villageois avec le nombre de réservations qu'ils ont effectué
     * ainsi que le délai de réservation moyen sur une année OU une période
     *
     * @param int    $year   L'année sur laquelle on veut obtenir les réservations
     * @param string $dtDeb  La date de début d'une période
     * @param string $dtFin  La date de fin d'une période
     *
     * @return array
     */
    public function get_liste_villageois($year = 'null', $dtDeb = '01-05-2016', $dtFin = '01-12-2016')
    {
        $this->db->select('user, nomvillageois, prenomvillageois, adrmail, notel, noport, count(resa.noheb) as nb_resa, SUM(datediff(DATEDEBSEM, DATERESA))/count(noheb) as delai_achat_moyen ')
            ->from('villageois')
            ->join('resa', 'resa.novillageois = villageois.novillageois', 'left')
            ->group_by('villageois.novillageois');
        if ($year != 'null') {
            $this->db->where('YEAR(dateresa)', $year);
        } elseif ($dtDeb != '' && $dtFin != '') {
            $this->db->where('dateresa >=', date('Y-m-d', strtotime($dtDeb)));
            $this->db->where('dateresa <=', date('Y-m-d', strtotime($dtFin)));
        }
        return $this->db->get()->result();
    }

    /**
     * Ajout demandé lors de l'épreuve E4 du BTS SIO
     *
     * Récupère le délai moyen entre les réservations pour un villageois
     *
     * @param int $idVill L'identifiant du villageois
     * @return float
     */
    public function get_delais_achat_moyen($idVill)
    {
        return $this->db->select('SUM(datediff(DATEDEBSEM, DATERESA))/count(noheb) as delai_achat_moyen')
            ->from('resa')
            ->where('novillageois', $idVill)
            ->get()
            ->row()->delai_achat_moyen;
    }

    /**
     * Ajout d'un compte utilisateur
     *
     * @param string $user
     * @param string $nom
     * @param string $prenom
     * @param string $typeCompte
     * @return void
     */
    public function ajout_compte($user, $nom, $prenom, $typeCompte)
    {
        $this->db->set('user', $user)
            ->set('nomcompte', $nom)
            ->set('prenomcompte', $prenom)
            ->set('mdp', password_hash($user, PASSWORD_BCRYPT, ['cost' => 12]))
            ->set('typecompte', $typeCompte)
            ->set('dateinscrip', 'NOW()', false)
            ->insert('compte');
    }
}
