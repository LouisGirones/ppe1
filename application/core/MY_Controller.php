<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{

    protected $menu;

    public function __construct()
    {
        parent::__construct();

        //DEBUG
        $this->output->enable_profiler(true);

        //On recupere le menu correspondant au type de compte
        $this->menu = menu_url($this->session->userdata('user_type'));

    }

    /**
     * Affichage de la vue d'erreur
     *
     * @param string $msg Le message d'erreur
     * @return void
     */
    public function vue_erreur($msg)
    {
        //Titre de la page
        $data['titre'] = 'Erreur';
        $data['description'] = "";
        $data['erreur'] = alert('danger', 'Erreur', $msg);
        //Preparation des vues
        $menu = $this->load->view($this->menu, null, true);
        $contenu = $this->load->view('erreur', $data, true);
        //Chargement des vues dans le template
        $this->load->view('template', ['menu' => $menu, 'contenu' => $contenu]);
    }
}
