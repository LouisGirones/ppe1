<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Gestionnaire extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        //Check de la session
        if ($this->session->userdata('user_type') !== "GES") {
            redirect(base_url());
            return;
        }
        $this->load->model('reservation_model');
    }

    /**
     * Listes des réservations, possibilité de les modifier
     *
     * @return void
     */
    public function index()
    {
        //Var de recherches
        $etat = !empty($_GET['etatRecherche']) ? (int) $_GET['etatRecherche'] : '';
        $semaine = !empty($_GET['semaineRecherche']) ? $_GET['semaineRecherche'] : '';
        //Lien pour que la pagination suive la recherche
        $chaine = !empty($etat) ? '?etatRecherche=' . $etat : '';
        $chaine .= !empty($semaine) ? '?semaineRecherche=' . $semaine : '';

        //Chargement des bibliotheques
        $this->load->library('form_validation');
        //Chargement des helpers
        $this->load->helper(['form', 'html']);

        $noHeb = $this->input->post('no_heb');
        $semDeb = $this->input->post('dt_deb_sem');

        $id = $noHeb . '_' . $semDeb;
        $nvEtatResa = $this->input->post('nv_etat_resa_' . $id);

        $this->form_validation->set_rules("no_heb", "", "trim|required|integer");
        $this->form_validation->set_rules("dt_deb_sem", "", "trim|required|date_valide_check");
        $this->form_validation->set_rules("nv_etat_resa_" . $id, "", "trim|required|integer");

        switch ($nvEtatResa) {
            case '2':
                // accuse recept
                $this->form_validation->set_rules("txt_date_acc_recept_" . $id, "accusé de réception", "trim|required|date_valide_check");
                break;
            case '3':
                // arrhes
                $this->form_validation->set_rules("txt_date_arrhes_" . $id, "date de réception des arrhes", "trim|required|date_valide_check");
                $this->form_validation->set_rules("txt_mt_arrhes_" . $id, "montant des arrhes", "trim|required|greater_than[1]|less_than[501]");
                break;
            case '4':
                // arrivée du villageois
                $this->form_validation->set_rules("dt_deb_sem", "", "date_superieur_ojd");
                break;
            default:
                # code...
                break;
        }

        if ($this->form_validation->run()) {
            $page = $this->uri->segment(3) > 0 ? '/' . $this->uri->segment(3) : '';
            switch ($nvEtatResa) {
                case '2':
                    // accuse recept
                    $dtAccRecept = $this->input->post('txt_date_acc_recept_' . $id);
                    $this->reservation_model->set_date_acc_recept($dtAccRecept, $nvEtatResa, $noHeb, $semDeb);
                    $this->session->set_flashdata('msg_' . $id, alert('success', 'Confirmation ', 'Date d\'accusée de réception enregistrée'));
                    redirect(site_url('/gestionnaire/index' . $page . $chaine . '#' . $noHeb . $semDeb));
                    break;
                case '3':
                    // arrhes
                    $dtArrhes = $this->input->post('txt_date_arrhes_' . $id);
                    $mtArrhes = $this->input->post('txt_mt_arrhes_' . $id);
                    $this->reservation_model->set_date_mt_arrhes($dtArrhes, $mtArrhes, $nvEtatResa, $noHeb, $semDeb);
                    $this->session->set_flashdata('msg_' . $id, alert('success', 'Confirmation ', 'Date et montant des arrhes enregistrée'));
                    redirect(site_url('/gestionnaire/index' . $page . $chaine . '#' . $noHeb . $semDeb));
                    break;
                case '4':
                    // arrivée du villageois
                    $this->reservation_model->set_resa_en_cours($nvEtatResa, $noHeb, $semDeb);
                    $this->session->set_flashdata('msg_' . $id, alert('success', 'Confirmation ', 'Réservation en cours'));
                    redirect(site_url('/gestionnaire/index' . $page . $chaine . '#' . $noHeb . $semDeb));
                    break;
                default:
                    # code...
                    break;
            }
        }
        //Pagination
        $this->load->library('pagination');
        $config['base_url'] = site_url() . '/gestionnaire/index/';
        $config['total_rows'] = $this->reservation_model->get_nb_resas('', false, $etat, $semaine);
        $config['per_page'] = 3;
        $config['num_links'] = '1';
        $config['use_page_numbers'] = true;
        $config["uri_segment"] = 3;
        //Initialisation pagination
        $this->pagination->initialize($config);
        //Calcul de l'offset
        $offset = $this->uri->segment(3) > 0 ? ($this->uri->segment(3)) * $config['per_page'] - $config['per_page'] : $offset = $this->uri->segment(3);
        //Données des vues
        add_js(['jquery-ui.min', 'datepicker']);
        add_css('jquery-ui.min');
        $data['titre'] = 'Liste des réservations';
        $data['description'] = "";
        $data['links'] = $this->pagination->create_links();
        $data['resas'] = $this->reservation_model->get_listes_resas('', $config["per_page"], $offset, false, $etat, $semaine);
        $data['etats_resa'] = $this->reservation_model->get_etats_resa();
        $data['chaine'] = $chaine;
        //Preparation des vues
        $contenu = $this->load->view('gestionnaire/liste_resa', $data, true);
        $menu = $this->load->view($this->menu, null, true);
        //Chargement des vues dans le template
        $this->load->view('template', ['menu' => $menu, 'contenu' => $contenu]);
    }

    /**
     * Liste des réservations passées
     *
     * @return void
     */
    public function historique_reservations()
    {
        //Chargement des bibliotheques
        $this->load->library('form_validation');
        //Chargement des helpers
        $this->load->helper(['form', 'html']);
        //Pagination
        $this->load->library('pagination');
        $config['base_url'] = base_url() . '/gestionnaire/historique_reservations/';
        $config['total_rows'] = $this->reservation_model->get_nb_resas('', true);
        $config['per_page'] = 3;
        $config['num_links'] = '1';
        $config['use_page_numbers'] = true;
        $config["uri_segment"] = 3;
        //Initialisation pagination
        $this->pagination->initialize($config);
        //Calcul de l'offset
        $offset = $this->uri->segment(3) > 0 ? ($this->uri->segment(3)) * $config['per_page'] - $config['per_page'] : $offset = $this->uri->segment(3);
        //Données des vues
        $data['titre'] = 'Historique des réservations';
        $data['description'] = "";
        $data['links'] = $this->pagination->create_links();
        $data['resas'] = $this->reservation_model->get_listes_resas('', $config["per_page"], $offset, true);
        $data['etats_resa'] = $this->reservation_model->get_etats_resa();
        //Preparation des vues
        $contenu = $this->load->view('gestionnaire/liste_resa', $data, true);
        $menu = $this->load->view($this->menu, null, true);
        //Chargement des vues dans le template
        $this->load->view('template', ['menu' => $menu, 'contenu' => $contenu]);
    }

    /**
     * Supprimer une réservation, uniquement possible pour les réservations dans l'historique
     *
     * @param int    $noheb L'identifiant d'hébergement
     * @param string $dtdeb La date de début de la réservation
     * @return void
     */
    public function supprimer_resa($noheb, $dtdeb)
    {
        $nbRow = $this->reservation_model->supprimer_resa_gest($noheb, $dtdeb);
        if ($nbRow == 1) {
            $this->session->set_flashdata('msg', alert('success', 'Confirmation ', 'La réservation a bien était supprimée.'));
        } else {
            $this->session->set_flashdata('msg', alert('warning', 'Attention ', 'La réservation n\'a pas pu être supprimée.'));
        }
        redirect(site_url('gestionnaire/'));
    }
}
