<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ajax extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        //Chargement du model
        $this->load->model('ajax_model');
    }

    /**
     * Récupère les noms d'hébergements qui "match" la chaîne entrée
     *
     * @todo utiliser le form validation
     * @todo passer en post
     * @return json
     */
    public function get_noms_hebergs()
    {
        //Le get de jquery ui pour .autocomplete
        if (!empty($_GET['term'])) {
            $nom = $_GET['term'];
            $tbNom = [];
            //TODO : utiliser la méthode run de form_validation
            $result = $this->ajax_model->get_noms_hebergs($nom);
            foreach ($result as $row) {
                $tbNom[] = $row->nomheb;
            }
            echo json_encode($tbNom);
        }
    }

    /**
     * Récupère les semaines de réservations possibles
     *
     * @return json
     */
    public function get_semaines()
    {
        $this->load->library('form_validation');
        $this->load->helper(['form', 'html']);

        $this->form_validation->set_rules("noHeb", "no heb", "trim|required|integer");
        $this->form_validation->set_rules("saison", "saison", "trim|required|integer");

        if ($this->form_validation->run()) {
            $noHeb = $this->input->post("noHeb");
            $cdSaison = $this->input->post("saison");
            //Recupération des semaines
            $tbSemaines = [];
            $result = $this->ajax_model->get_semaines_dispo($noHeb, $cdSaison);
            foreach ($result as $row) {
                $tbSemaines[] = $row->datedebsem;
            }
            echo json_encode($tbSemaines);
        } else {
            echo json_encode("Impossible de récuperer les semaines");
        }
    }
}
