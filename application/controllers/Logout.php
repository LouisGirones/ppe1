<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Logout extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        //Si l'utilisateur n'est pas loggé on le redirige
        if ($this->session->userdata('logged_user') == false) {
            redirect(base_url());
        }
    }

    /**
     * Déconnexion et redirection sur l'accueil
     *
     * @return void
     */
    public function index()
    {
        //Destruction de la session
        session_destroy();
        //Redirection sur l'accueil
        redirect(base_url());
    }
}
