<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Accueil extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Page d'accueil du site
     *
     * @return void
     */
    public function index()
    {
        $data['titre'] = "Accueil";
        $data['description'] = "Accueil du site de réservation Village Vacances Alpes";
        $contenu = $this->load->view('accueil', $data, true);
        $menu = $this->load->view($this->menu, null, true);
        $this->load->view('template', ['menu' => $menu, 'contenu' => $contenu]);
    }
}
