<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Connexion extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        //Si l'utilisateur est déja connecté on le redirige sur l'accueil
        if (!empty($this->session->userdata('logged_user'))) {
            redirect(base_url());
            return;
        }
    }

    /**
     * Connexion à l'application
     *
     * @return void
     */
    public function index()
    {
        $this->load->library('form_validation');
        $this->load->helper(['form', 'html']);
        $this->load->model('compte_model');

        //Règles de validations
        $this->form_validation->set_rules("txt_login", "Login", "trim|required");
        $this->form_validation->set_rules("txt_password", "Mot de passe", "trim|required|max_length[200]|min_length[5]");

        if ($this->form_validation->run()) {
            //Recuperation des valeurs du formulaire
            $login = $this->input->post("txt_login");
            $password = $this->input->post("txt_password");
            if ($this->compte_model->connect($login, $password)) {
                //Récupération des informations sur le compte
                $user = $this->compte_model->get_infos_compte($login);

                //Tableau de session
                $sessiondata = [
                    'logged_user' => true,
                    'user_login' => $login,
                    'user_type' => $user->typecompte,
                    'nom_compte' => $user->nomcompte,
                    'prenom_compte' => $user->prenomcompte,
                    'date_inscript' => $user->dateinscrip,
                ];
                //Si le compte est un villageois
                if ($user->typecompte === 'VIL') {
                    //Chargement du modèle
                    $this->load->model('villageois_model');
                    $villageois = $this->villageois_model->get_infos_villageois($login);
                    //Données de sessions pour un villageois
                    $sessiondata['num_villageois'] = $villageois->novillageois;
                    $sessiondata['nom_villageois'] = $villageois->nomvillageois;
                    $sessiondata['prenom_villageois'] = $villageois->prenomvillageois;
                    $sessiondata['mail_villageois'] = $villageois->adrmail;
                    $sessiondata['tel_villageois'] = $villageois->notel;
                }
                //Enregistrement en session
                $this->session->set_userdata($sessiondata);
                //Redirection sur l'accueil
                redirect(base_url());
            } else {
                $this->session->set_flashdata('msg', alert('danger', 'Attention ', 'Identifiants invalides'));
                redirect(site_url('connexion/index'));
            }
        }
        //Affichage de la vue
        $data['titre'] = "Connexion";
        $data['description'] = "";
        $menu = $this->load->view($this->menu, null, true);
        $contenu = $this->load->view('connexion', $data, true);
        $this->load->view('template', ['menu' => $menu, 'contenu' => $contenu]);
    }
}
