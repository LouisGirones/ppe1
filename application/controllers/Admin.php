<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        //Vérification des droits
        if ($this->session->userdata('user_type') !== "AD") {
            redirect(base_url());
            return;
        }
    }

    /**
     * Accueil du controleur d'administration
     *
     * @return void
     */
    public function index()
    {
        $this->load->model(['compte_model', 'saison_model']);
        $data['titre'] = "Gestion des données";
        $data['description'] = "";
        $data['comptes'] = $this->compte_model->get_liste_comptes();
        $data['saisons'] = $this->saison_model->get_liste_saisons();
        $contenu = $this->load->view('admin/gestion_bd', $data, true);
        $menu = $this->load->view($this->menu, null, true);
        $this->load->view('template', ['menu' => $menu, 'contenu' => $contenu]);
    }

    /**
     *
     * Ajout demandé lors de l'épreuve E4 du BTS SIO
     *
     * Affichage des villageois avec leurs nombre de réservation,
     * la moyenne des délais entre chaque réservations. Possibilité
     * de trier par année OU date de début et fin.
     *
     * @param int     $annee l'année de recherche de réservations
     * @param string  $dtDeb date de début
     * @param string  $dtFin date de fin
     * @return void
     */
    public function villageois($annee = 'null', $dtDeb = '', $dtFin = '')
    {
        $this->load->library('form_validation');
        $this->load->helper(['form', 'html']);
        $this->load->model('compte_model');

        //assets
        add_js(['jquery-ui.min', 'datepicker']);
        add_css('jquery-ui.min');

        //Règles de validations
        $this->form_validation->set_rules("txt_annee", "annee", "trim|integer");
        $this->form_validation->set_rules("txt_semaine_deb", "semaine de début", "trim|date_valide_check");
        $this->form_validation->set_rules("txt_semaine_fin", "semaine de fin", "trim|date_valide_check");

        if ($this->form_validation->run()) {
            $annee_post = $this->input->post("txt_annee");
            $dt_deb_post = $this->input->post("txt_semaine_deb");
            $dt_fin_post = $this->input->post("txt_semaine_fin");

            if ($annee_post != '') {
                redirect(site_url('admin/villageois/' . $annee_post));
            }

            if ($dt_deb_post != '' && $dt_fin_post != '') {
                redirect(site_url('admin/villageois/null/' . $dt_deb_post . '/' . $dt_fin_post));
            }
        }
        $data['titre'] = "Gestion des villageois";
        $data['description'] = "";
        $data['comptes'] = $this->compte_model->get_liste_villageois($annee, $dtDeb, $dtFin);
        $contenu = $this->load->view('admin/villageois', $data, true);
        $menu = $this->load->view($this->menu, null, true);
        $this->load->view('template', ['menu' => $menu, 'contenu' => $contenu]);
    }

    /**
     * Ajout d'un compte de type villageois ou gestionnaire
     *
     * @return void
     */
    public function ajout_compte()
    {
        $this->load->library('form_validation');
        $this->load->helper(['form', 'html']);

        $typeCompte = $this->input->post("type_compte");

        $this->form_validation->set_rules("txt_login", "Identifiant", "required|is_unique[compte.user]|min_length[5]|max_length[8]",
            ['is_unique' => 'Ce login est déjà enregistrée.']);
        $this->form_validation->set_rules("type_compte", "Type de compte", "required|in_list[VIL,GES]");
        $this->form_validation->set_rules("txt_nom", "Nom", "trim|required|max_length[40]|min_length[3]");
        $this->form_validation->set_rules("txt_prenom", "Prenom", "trim|required|max_length[40]|min_length[3]");
        //Règles additionnels
        if ($typeCompte === 'VIL') {
            $this->form_validation->set_rules("txt_mail", "Adresse mail", "required|valid_email|max_length[50]|is_unique[villageois.adrmail]",
                ['is_unique' => 'Cette adresse mail est déjà enregistrée.']);
            $this->form_validation->set_rules("txt_tel_fixe", "Tél. fixe", "trim|numeric|exact_length[10]");
            $this->form_validation->set_rules("txt_tel_port", "Tél. portable", "trim|numeric|exact_length[10]");
        }

        if ($this->form_validation->run()) {
            $this->load->model(['compte_model', 'villageois_model']);

            //Recuperation des valeurs du formulaire
            $login = $this->input->post("txt_login");
            $nom = $this->input->post("txt_nom");
            $prenom = $this->input->post("txt_prenom");

            //Enregistrement du compte
            $this->compte_model->ajout_compte($login, $nom, $prenom, $typeCompte);

            //Enregistrement du villageois
            if ($typeCompte === 'VIL') {
                $mail = $this->input->post("txt_mail");
                $telFixe = $this->input->post("txt_tel_fixe");
                $telPort = $this->input->post("txt_tel_port");
                $this->villageois_model->ajout_villageois($login, $mail, $telFixe, $telPort);
            }
            //Redirection sur l'accueil
            $type = $typeCompte === 'VIL' ? 'villageois' : 'gestionnaire';
            $this->session->set_flashdata('msg', alert('success', 'Confirmation', 'Le compte ' . $type . ' ' . $login . ' a bien était ajouté.'));
            redirect(site_url("admin/ajout_compte"));
        }
        //Affichage de la vue
        $data['titre'] = "Ajouter un compte";
        $data['description'] = "";
        $menu = $this->load->view($this->menu, null, true);
        $contenu = $this->load->view('admin/ajout_membre', $data, true);
        $this->load->view('template', ['menu' => $menu, 'contenu' => $contenu]);
    }

    /**
     * Ajout d'une saison, vérification qu'une saison n'est pas déjà en cours
     *
     * @return void
     */
    public function ajout_saison()
    {
        $this->load->model('saison_model');
        $this->load->library('form_validation');
        $this->load->helper(['form', 'html']);

        $this->form_validation->set_rules("txt_nom_saison", "Nom de la saison", "required|max_length[15]|is_unique[saison.nomsaison]",
            ['is_unique' => 'Une saison avec ce nom est déjà enregistrée.']);
        $this->form_validation->set_rules("dt_deb_saison", "Date de début", "trim|required|date_valide_check|date_couple_check|saison_existe_check");
        $this->form_validation->set_rules("dt_fin", "Date de fin", "trim|required|date_valide_check");

        if ($this->form_validation->run()) {
            $nom_saison = $this->input->post("txt_nom_saison");
            $dtDeb = $this->input->post("dt_deb_saison");
            $dtFin = $this->input->post("dt_fin");
            //enregistrement de la saison
            $codeSaison = $this->saison_model->ajouter_saison($nom_saison, $dtDeb, $dtFin);
            //enregistrement des semaines de la saison
            $nbSem = $this->saison_model->ajouter_semaines_saison($codeSaison, $dtDeb, $dtFin);
            //Redirection sur l'accueil
            $this->session->set_flashdata('msg', alert('success', 'Confirmation', 'La saison ' . $nom_saison . ' a bien était ajouté, elle comporte ' . $nbSem . ' semaines.'));
            redirect(site_url("admin/ajout_saison"));
        }

        //Affichage de la vue
        add_js(['jquery-ui.min', 'datepicker']);
        add_css('jquery-ui.min');
        $data['titre'] = "Ajouter une saison";
        $data['description'] = "";
        $menu = $this->load->view($this->menu, null, true);
        $contenu = $this->load->view('admin/ajout_saison', $data, true);
        $this->load->view('template', ['menu' => $menu, 'contenu' => $contenu]);
    }
}
