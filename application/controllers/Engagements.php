<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Engagements extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Présentation des engagements
     *
     * @return void
     */
    public function index()
    {
        //Titre de la page
        $data['titre'] = "Engagements";
        //Description de la page
        $data['description'] = "Engagements de Village Vacances Alpes";
        //Preparation des vues
        $contenu = $this->load->view('engagements', $data, true);
        $menu = $this->load->view($this->menu, null, true);
        //Chargement des vues dans le template
        $this->load->view('template', ['menu' => $menu, 'contenu' => $contenu]);
    }
}
