<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Hebergement extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        //Chargement des modeles
        $this->load->model('hebergement_model');
    }

    /**
     * Liste des hébergements
     *
     * @return void
     */
    public function index()
    {
        //Pagination
        $this->load->library('pagination');
        $config['base_url'] = site_url() . '/hebergement/index/';
        $config['total_rows'] = $this->hebergement_model->get_nb_heberg();
        $config['per_page'] = 6;
        $config['num_links'] = '1';
        $config['use_page_numbers'] = true;
        $config["uri_segment"] = 3;
        //Initialisation pagination
        $this->pagination->initialize($config);
        //Calcul de l'offset
        $offset = $this->uri->segment(3) > 0 ? ($this->uri->segment(3)) * $config['per_page'] - $config['per_page'] : $offset = $this->uri->segment(3);
        //Vue
        $data['titre'] = 'Liste des hébergements';
        $data['description'] = "";
        $data['links'] = $this->pagination->create_links();
        $data['hebergs'] = $this->hebergement_model->get_hebergs($config["per_page"], $offset);
        $menu = $this->load->view($this->menu, null, true);
        //Preparation des vues
        $contenu = $this->load->view('hebergement/liste_heberg', $data, true);
        $menu = $this->load->view($this->menu, null, true);
        //Chargement des vues dans le template
        $this->load->view('template', ['menu' => $menu, 'contenu' => $contenu]);
    }

    /**
     * Informations sur un hébergement
     *
     * @param int   $id l'identifiant de l'hébergement
     * @return void
     */
    public function voir_heberg($id = '')
    {
        //L'id n'est pas un entier
        if ((int) $id == 0) {
            $this->vue_erreur('Identifiant d\'hebergement invalide.');
            return;
        }
        //Recupération des données de l'heberg
        $data['heberg'] = $this->hebergement_model->get_details_heberg($id);
        //L'heberg n'existe pas
        if (empty($data['heberg'])) {
            $this->vue_erreur('Cet hebergement n\'existe pas');
            return;
        }

        //Uniquement utilisé par les comptes GES et VIL
        $this->load->model('saison_model');
        $data['prix_saisons'] = $this->saison_model->ListePrixSaison($id);

        if ($this->session->userdata('user_type') === 'GES') {
            //Ajout de prix a cet heberg pour une saison
            $this->set_prix_saison($id);
        }
        //Préparation des vues
        $data['titre'] = 'Hebergement ' . $data['heberg']->nomheb;
        $data['description'] = "";
        //Chargement des vues
        $menu = $this->load->view($this->menu, null, true);
        $contenu = $this->load->view('hebergement/heberg', $data, true);
        //Template
        $this->load->view('template', ['menu' => $menu, 'contenu' => $contenu]);
    }

    /**
     * Recherche d'un hébergement avec autocomplétion
     *
     * @return void
     */
    public function rechercher_heberg()
    {
        //Chargement des bibliotheques
        $this->load->library('form_validation');
        //Chargement des helpers
        $this->load->helper(['form', 'html']);
        //Ajout de l'autocompletion jquery ui
        add_js(['jquery-ui.min', 'autocompletion']);
        add_css('jquery-ui.min');

        //Règles de validations
        $this->form_validation->set_rules("txt_nomheb", "nom de l'hébergement", "trim|required");

        if ($this->form_validation->run()) {
            $nom = $this->input->post("txt_nomheb");
            $noheb = $this->hebergement_model->get_id_heberg_par_nom($nom);
            if ((int) $noheb !== 0) {
                redirect(site_url('hebergement/voir_heberg/' . $noheb));
            } else {
                $this->session->set_flashdata('msg', alert('warning', 'Alerte ', 'Aucun hébergement s\'appelant "' . $nom . '" n\'existe.'));
                redirect(site_url('hebergement/rechercher_heberg'));
            }
        }
        $data['titre'] = "Recherche d'hébergement";
        $data['description'] = "";
        $menu = $this->load->view($this->menu, null, true);
        $contenu = $this->load->view('hebergement/chercher_heberg', $data, true);
        $this->load->view('template', ['menu' => $menu, 'contenu' => $contenu]);
    }

    /**
     * Modification et ajout d'un hébergement, uniquement possible pour le gestionnaire
     *
     * @param bool $modif Si on modifie ou ajoute un hébergement
     * @param int  $id    L'identifiant de l'hébergement à modifier
     * @return void
     */
    public function update_hebergement($modif = false, $id = null)
    {
        if ($this->session->userdata('user_type') !== "GES") {
            redirect(base_url());
        }

        if (true == $modif && empty($id)) {
            $this->vue_erreur('Identifiant d\'hebergement invalide.');
            return;
        }

        $this->load->library('form_validation');
        //Chargement des helpers
        $this->load->helper(['form', 'html']);
        if (true == $modif && !empty($id)) {
            $data['heberg'] = $this->hebergement_model->get_details_heberg($id);
            //L'heberg n'existe pas
            if (null == $data['heberg']) {
                $this->vue_erreur('Cet hebergement n\'existe pas');
                return;
            }
            $this->form_validation->set_rules("txt_nom", "Nom de l'hébergement", "nom_heberg_libre");
        } else {
            $this->form_validation->set_rules("txt_nom", "Nom de l'hébergement", "is_unique[hebergement.nomheb]");
        }

        $this->form_validation->set_rules("txt_nom", "Nom de l'hébergement", "trim|required|max_length[25]",
            ['is_unique' => 'Un hébergement avec ce nom est déjà enregistré.']);
        $this->form_validation->set_rules("txt_nb_place", "Nombre de place", "trim|required|integer|greater_than[0]|less_than[11]");
        $this->form_validation->set_rules("txt_surface", "Surface", "trim|required|integer|greater_than[14]|less_than[200]");
        $this->form_validation->set_rules("txt_type_heb", "Type d'hébergement", "trim|required|in_list[1,2,3]");
        $this->form_validation->set_rules("txt_internet", "Internet", "trim|required|in_list[0,1]");
        $this->form_validation->set_rules("txt_annee", "Annee", "trim|required|integer|greater_than[2005]|less_than[2017]");
        $this->form_validation->set_rules("txt_etat", "Etat", "trim|required|max_length[32]");
        $this->form_validation->set_rules("txt_secteur", "Secteur", "trim|required|max_length[15]");
        $this->form_validation->set_rules("txt_orientation", "Orientation", "trim|required|max_length[5]");
        $this->form_validation->set_rules("txt_description", "Description", "trim|required|max_length[900]");
        //Le formulaire contient des erreurs
        if ($this->form_validation->run()) {
            $nom = $this->input->post("txt_nom");
            $nbPlace = $this->input->post("txt_nb_place");
            $surface = $this->input->post("txt_surface");
            $type_heb = $this->input->post("txt_type_heb");
            $internet = $this->input->post("txt_internet");
            $annee = $this->input->post("txt_annee");
            $etat = $this->input->post("txt_etat");
            $secteur = $this->input->post("txt_secteur");
            $orientation = $this->input->post("txt_orientation");
            $description = $this->input->post("txt_description");
            //On regarde si il y a un fichier à upload
            if (!empty($_FILES['photo']['name'])) {
                $config['upload_path'] = './assets/img/uploads/imgHebergement/' . $this->hebergement_model->get_nom_type_heberg($type_heb);
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '2048';
                $config['max_width'] = '3200';
                $config['max_height'] = '2400';
                $config['overwrite'] = true;
                $config['file_name'] = "$nom." . pathinfo($_FILES['photo']['name'], PATHINFO_EXTENSION);

                $this->load->library('upload', $config);
                //Impossible de charger le fichier
                if (!$this->upload->do_upload("photo")) {
                    $data['titre'] = "Ajouter un hébergement";
                    $data['description'] = "";
                    $data['types_heberg'] = $this->hebergement_model->get_types_heberg();
                    $data['error_upload'] = $this->upload->display_errors();
                    //var_dump(expression)
                    $data['etat'] = $this->get_etats();
                    $data['secteur'] = $this->get_secteurs();
                    $data['orientation'] = $this->get_orientations();
                    $menu = $this->load->view($this->menu, null, true);
                    $contenu = $this->load->view('gestionnaire/update_heberg', $data, true);
                    $this->load->view('template', ['menu' => $menu, 'contenu' => $contenu]);
                } else {
                    $data = ['upload_data' => $this->upload->data()];
                }
            }

            if (!isset($data['error_upload'])) {
                //si une image existe
                $nomImg = isset($data['upload_data']['file_name']) ? $data['upload_data']['file_name'] : null;
                if (false == $modif) {
                    $id_heb = $this->hebergement_model->ajouter_hebergement($nom, $type_heb, $nbPlace, $surface, $internet, $annee, $secteur, $orientation, $etat, $description, $nomImg);
                    $this->session->set_flashdata('msg_heberg', alert('success', 'Confirmation ', 'L\'hébergement à bien été ajouté'));
                    redirect(site_url('/hebergement/voir_heberg/' . $id_heb));
                } else {
                    $this->hebergement_model->modifier_hebergement($id, $nom, $type_heb, $nbPlace, $surface, $internet, $annee, $secteur, $orientation, $etat, $description, $nomImg);
                    $this->session->set_flashdata('msg_heberg', alert('success', 'Confirmation ', 'L\'hébergement à bien été modifié'));

                    redirect(site_url('/hebergement/voir_heberg/' . $id));
                }
            }
        }
        $data['modif'] = $modif;
        $data['description'] = "";
        $data['etat'] = $this->get_etats();
        $data['orientation'] = $this->get_orientations();
        $data['secteur'] = $this->get_secteurs();
        $data['types_heberg'] = $this->hebergement_model->get_types_heberg();
        $menu = $this->load->view($this->menu, null, true);

        if (false == $modif) {
            //Affichage de la vue
            $data['titre'] = "Ajouter un hébergement";
        } else {
            $data['titre'] = "Modifier un hébergement";
        }
        $contenu = $this->load->view('gestionnaire/update_heberg', $data, true);
        $this->load->view('template', ['menu' => $menu, 'contenu' => $contenu]);
    }

    /**
     * Enregistrer le prix d'un hébergement pour une saison donnée, uniquement possible pour le gestionnaire
     *
     * @param int   $id L'id de l'hébergement
     * @return void
     */
    private function set_prix_saison($id)
    {
        //Chargement des bibliotheques
        $this->load->library('form_validation');
        //Chargement des helpers
        $this->load->helper(['form', 'html']);
        //Recuperation des valeurs du formulaire

        $prix = $this->input->post("txt_prix_saison");
        $noHeb = $this->input->post("noHeb");
        $cdSaison = $this->input->post("cdSaison");
        $this->form_validation->set_rules("txt_prix_saison", "Prix", "trim|required|integer");
        $this->form_validation->set_rules("noHeb", "noheb", "trim|required|integer");
        $this->form_validation->set_rules("cdSaison", "cdSaison", "trim|required|integer");
        //Le formulaire ne contient pas d'erreur
        if (!$this->form_validation->run() == false) {
            $this->hebergement_model->set_prix_saison($noHeb, $cdSaison, $prix);
            $this->session->set_flashdata('msg_tarif', alert('success', '', 'Le prix à bien été enregistré.'));
            redirect(site_url('/hebergement/voir_heberg/' . $id));
        }
    }

    /**
     * Renvoie les états possibles d'un hébergement
     *
     * @return array
     */
    private function get_etats()
    {
        return [
            0 => "Très bon",
            1 => "Bon",
            2 => "Moyen",
            3 => "En attente de rénovation",
            4 => "En cours de rénovation",
        ];
    }

    /**
     * Renvoie les secteurs possibles pour un hébergement
     *
     * @return array
     */
    private function get_secteurs()
    {
        return [
            0 => "Colline",
            1 => "Montagne",
            2 => "Falaise",
            3 => "Alpes",
        ];
    }

    /**
     * Renvoie les orientations possibles pour un hébergement
     *
     * @return array
     */
    private function get_orientations()
    {
        return [
            0 => "Nord",
            1 => "Sud",
            2 => "Est",
            3 => 'Ouest',
        ];
    }
}
