<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Villageois extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        //Check de la session
        if ($this->session->userdata('user_type') !== "VIL") {
            redirect(base_url());
            return;
        }
        $this->load->model('reservation_model');
    }

    /**
     * Compte d'un villageois, avec affichage de la liste de ses réservations
     *
     * @return void
     */
    public function index()
    {
        //Identifiant du villageois
        $id = (int) $this->session->userdata('num_villageois');
        //Pagination
        $this->load->library('pagination');
        $config['base_url'] = site_url() . '/villageois/index/';
        $config['total_rows'] = $this->reservation_model->get_nb_resas($id);
        $config['per_page'] = 3;
        $config['num_links'] = '1';
        $config['use_page_numbers'] = true;
        $config["uri_segment"] = 3;
        //Initialisation pagination
        $this->pagination->initialize($config);
        //Calcul de l'offset
        $offset = $this->uri->segment(3) > 0 ? ($this->uri->segment(3)) * $config['per_page'] - $config['per_page'] : $offset = $this->uri->segment(3);
        //Données des vues
        $data['titre'] = 'Votre compte';
        $data['description'] = "";
        $data['links'] = $this->pagination->create_links();
        $data['resas'] = $this->reservation_model->get_listes_resas($id, $config["per_page"], $offset);
        //Preparation des vues
        $contenu = $this->load->view('villageois/compte', null, true);
        $contenu .= $this->load->view('villageois/liste_resas', $data, true);
        $menu = $this->load->view($this->menu, null, true);
        //Chargement des vues dans le template
        $this->load->view('template', ['menu' => $menu, 'contenu' => $contenu]);
    }

    /**
     * Historique des réservations d'un villageois
     *
     * @return void
     */
    public function historique_reservations()
    {
        //Identifiant du villageois
        $id = (int) $this->session->userdata('num_villageois');
        //Pagination
        $this->load->library('pagination');
        $config['base_url'] = base_url() . '/villageois/historique_reservations/';
        $config['total_rows'] = $this->reservation_model->get_nb_resas($id);
        $config['per_page'] = 3;
        $config['num_links'] = '1';
        $config['use_page_numbers'] = true;
        $config["uri_segment"] = 3;
        //Initialisation pagination
        $this->pagination->initialize($config);
        //Calcul de l'offset
        $offset = $this->uri->segment(3) > 0 ? ($this->uri->segment(3)) * $config['per_page'] - $config['per_page'] : $offset = $this->uri->segment(3);
        //Données des vues
        $data['titre'] = 'Historique de vos réservations';
        $data['description'] = "";
        $data['links'] = $this->pagination->create_links();
        $data['resas'] = $this->reservation_model->get_listes_resas($id, $config["per_page"], $offset, true);
        //Preparation des vues
        $contenu = $this->load->view('villageois/compte', null, true);
        $contenu .= $this->load->view('villageois/liste_resas', $data, true);
        $menu = $this->load->view($this->menu, null, true);
        //Chargement des vues dans le template
        $this->load->view('template', ['menu' => $menu, 'contenu' => $contenu]);
    }

    /**
     * Réservation d'un hébergement
     *
     * @param int   $noHeb       L'identifiant de l'hébergement
     * @param int   $codeSaison  Le code de la saison
     * @return void
     */
    public function reserver_heberg($noHeb = 0, $codeSaison = 0)
    {
        //Chargement des bibliotheques
        $this->load->model(['hebergement_model', 'saison_model', 'tarif_model', 'compte_model']);
        $data['heberg'] = $this->hebergement_model->get_details_heberg($noHeb);
        $data['saison'] = $this->saison_model->get_infos_saison($codeSaison);
        $data['prix'] = $this->tarif_model->get_tarif($noHeb, $codeSaison);
        if (null == $data['heberg'] || null == $data['saison']) {
            $this->vue_erreur('Impossible de réserver cet hébergement pour cette saison.');
            return;
        }

        if (null == $data['prix']) {
            $this->vue_erreur('Cet hébergement n\'a pas encore de prix pour cette saison.');
            return;
        }

        //Recup délai achat moyen
        $data['delais_moyen'] = $this->compte_model->get_delais_achat_moyen($this->session->userdata('num_villageois'));

        $this->load->library('form_validation');
        //Chargement des helpers
        $this->load->helper(['form', 'html']);
        //Recuperation des valeurs du formulaire

        $dtDeb = $this->input->post("txt_semaine_deb");
        $nbPers = $this->input->post("txt_nbpers");

        $this->form_validation->set_rules("txt_semaine_deb", "Date de début", "trim|required|date_valide_check|date_resa_check|heberg_libre");
        $this->form_validation->set_rules("txt_nbpers", "Nombre de personne", "trim|required|integer");

        //Le formulaire contient des erreurs
        if ($this->form_validation->run() == false) {
            //Affichage de la vue
            add_js(['jquery-ui.min', 'datepicker', 'ajax_semaines_resa_possible']);
            add_css('jquery-ui.min');
            $data['titre'] = "Réserver un hébergement";
            $data['description'] = "";
            $menu = $this->load->view($this->menu, null, true);
            $contenu = $this->load->view('villageois/reserver', $data, true);
            $this->load->view('template', ['menu' => $menu, 'contenu' => $contenu]);
        } else {
            $this->reservation_model->ajouter_resa($noHeb, $dtDeb, $this->session->userdata('num_villageois'), $nbPers, $data['prix']->prixheb);
            $this->session->set_flashdata('msg', alert('success', 'Confirmation ', 'La réservation a bien était ajoutée'));
            redirect(site_url('villageois/details_resa/' . $noHeb . '/' . $dtDeb));
        }
    }

    /**
     * Détails d'une réservation
     *
     * @param int    $noHeb L'identifiant de l'hébergement
     * @param string $dtDeb La date de début de la reservation
     * @return void
     */
    public function details_resa($noHeb, $dtDeb)
    {
        //Chargement des models
        $this->load->model('reservation_model');
        //Identifiant du villageois
        $id = (int) $this->session->userdata('num_villageois');
        $data['resa'] = $this->reservation_model->get_details_resa($id, $noHeb, $dtDeb);
        if (null == $data['resa']) {
            $this->vue_erreur('Cette réservation n\'existe pas.');
            return;
        }
        $data['titre'] = 'Détails d\'une réservation';
        $data['description'] = "";
        //Preparation des vues
        $contenu = $this->load->view('villageois/details_resa', $data, true);
        $menu = $this->load->view($this->menu, null, true);
        //Chargement des vues dans le template
        $this->load->view('template', ['menu' => $menu, 'contenu' => $contenu]);
    }

    /**
     * Supprimer une réservation
     *
     * @param int    $noHeb L'identifiant de l'hébergement
     * @param string $dtDeb La date de début de la réservation
     * @return void
     */
    public function supprimer_resa($noHeb, $dtDeb)
    {
        $this->load->model('reservation_model');
        $nbRow = $this->reservation_model->supprimer_resa($this->session->userdata('num_villageois'), $noHeb, $dtDeb);
        if (1 == $nbRow) {
            $this->session->set_flashdata('msg', alert('success', 'Confirmation ', 'La réservation a bien était supprimée'));
        } else {
            $this->session->set_flashdata('msg', alert('warning', 'Attention ', 'La réservation n\'a pas pu être supprimée. ' . $nbRow . ' réservation supprimée'));
        }
        redirect(site_url('villageois/'));
    }
}
