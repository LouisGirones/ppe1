<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation
{
    protected $CI;

    public function __construct()
    {
        parent::__construct();
        $this->CI = &get_instance();
    }

    /**
     * Vérifie qu'un champ est une date valide
     *
     * @todo utiliser l'objet date
     * @param string $date
     * @return bool
     */
    public function date_valide_check($date)
    {
        $this->CI->form_validation->set_message('date_valide_check', 'Cette date n\'est pas valide');
        $day = (int) substr($date, 0, 2);
        $month = (int) substr($date, 3, 2);
        $year = (int) substr($date, 6, 4);
        return checkdate($month, $day, $year);
    }

    /**
     * Date ultérieur à aujourd'hui
     *
     * @param string $date
     * @return bool
     */
    public function date_superieur_ojd($date)
    {
        $this->CI->form_validation->set_message('date_superieur_ojd', 'Les villageois ne peuvent prendre possession de leur hébergement avant la date de début');
        return strtotime("now") >= strtotime($date);
    }

    /**
     * Vérifie que l'on peut faire une réservation à une date donnée
     *
     * @param string $date
     * @return bool
     */
    public function date_resa_check($date)
    {
        $cdSaison = $this->CI->input->post("cdSaison");
        $resultat = $this->CI->reservation_model->verifier_semaine($date, $cdSaison);
        $this->CI->form_validation->set_message('date_resa_check', 'Impossible de réserver pour cette semaine');
        return $resultat == 1 ? true : false;
    }

    /**
     * Vérifie qu'un hébergement est libre à une semaine donnée
     *
     * @param string $date
     * @return bool
     */
    public function heberg_libre($date)
    {
        $noheb = $this->CI->input->post("noHeb");
        $resultat = $this->CI->reservation_model->verifier_heberg_libre($date, $noheb);
        $this->CI->form_validation->set_message('heberg_libre', 'Cet hébergement est déjà réservé pour cette semaine');
        return $resultat == 0 ? true : false;
    }

    /**
     * Vérifie que la date de début est antérieur à la date de fin
     *
     * @param string $date
     * @return bool
     */
    public function date_couple_check($date)
    {
        $dtFin = $this->CI->input->post("dt_fin");
        $this->CI->form_validation->set_message('date_couple_check', 'La date de début ne peut être postérieur à la date de fin');
        return strtotime($date) <= strtotime($dtFin);
    }

    /**
     * Vérifie si une saison est déjà en cours pour une période donnée
     *
     * @param string $date
     * @return bool
     */
    public function saison_existe_check($date)
    {
        $dt_fin = $this->CI->input->post("dt_fin");
        //$this->CI->load->model('ville_model');
        $resultat = $this->CI->saison_model->saison_existe($date, $dt_fin);
        $this->CI->form_validation->set_message('saison_existe_check', 'Une saison est déja en cours');
        return $resultat == 0 ? true : false;
    }

    /**
     * Vérifie qu'un nom d'hébergement est libre
     * @param string $nom
     * @return bool
     */
    public function nom_heberg_libre($nom)
    {
        $noheb = $this->CI->input->post("noHeb");
        $resultat = $this->CI->hebergement_model->verifier_nom_heberg_libre($nom, $noheb);
        $this->CI->form_validation->set_message('nom_heberg_libre', 'Ce nom d\'hébergement est déjà enregistré');
        return $resultat == 0 ? true : false;
    }
}
